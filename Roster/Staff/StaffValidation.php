<?php

namespace App\Classes\Staff\Roster\Staff;

class StaffValidation
{
    /* @var \App\Classes\Staff\Roster\Staff\Staff $staff */
    protected $staff;

    protected $maxMonthlyHours = self::DEFAULT_MONTHLY_MAX_HOURS;

    protected $maxWeeklyHours = self::DEFAULT_WEEKLY_MAX_HOURS;

   // [$each->license_id => $each->expiry_date, ...]
    protected $licenses;

    const DEFAULT_MONTHLY_MAX_HOURS = 160;
    const DEFAULT_WEEKLY_MAX_HOURS = 40;

    /**
     * @var \App\Classes\Staff\Roster\Staff\Staff $staff
     * @param $licenses
     */
    public function __construct($staff, $licenses){

        $this->staff = $staff;

        $this->licenses = $licenses;

        if ($staff && $staff->getUser() && $staff->getUser()->max_hours){
            $this->maxMonthlyHours = $staff->getUser()->max_hours;
        }

        $this->setWeeklyHours();

//        debug("WEEK: ".$this->maxWeeklyHours. " | MONTH: ".$this->maxMonthlyHours);
    }

    /**
     * Check Airport Licenses And Return False if NOT Satisfied
     * @param $airportLicenses
     * @param $serviceDate
     * @return bool
     */
    public function checkAirportLicenses($airportLicenses, $serviceDate){

        if (!count($airportLicenses)){
            return true;
        }

        if (!count($this->licenses)){
            return false;
        }

        foreach ($airportLicenses as $licenseId) {
            if (array_key_exists($licenseId, $this->licenses)) {
                // License is Expired
                if (strtotime($this->licenses[$licenseId]) < strtotime($serviceDate)){
//                    debug($this->staff->getUser()->last_name." ".$this->staff->getUser()->first_name.$this->licenses[$licenseId]." service date:". $serviceDate);
                    return false;
                }
            }
            else {
                return false;
            }
        }

        return true;
    }

    /**
     * Check Airline License
     * @param $service
     * @return bool
     */
    public function checkLicense($service){

        $airlineLicenseRequirements = $service->getLicenses();

        if (!count($airlineLicenseRequirements)){
            return true;
        }

        if (!count($this->licenses)){
            return false;
        }

        $serviceDate = $service->getReportDate();

        foreach ($airlineLicenseRequirements as $licenseId) {
            if (array_key_exists($licenseId, $this->licenses)) {
                // License is Expired
                if (strtotime($this->licenses[$licenseId]) < strtotime($serviceDate)){
                    return false;
                }
            }
            else {
                return false;
            }
        }

        return true;
    }

    public function setMaxMonthlyHours($value){
        $this->maxMonthlyHours = $value;
    }

    public function setWeeklyHours(){
        $this->maxWeeklyHours = $this->maxMonthlyHours ? intval($this->maxMonthlyHours / 4) : self::DEFAULT_WEEKLY_MAX_HOURS;
    }

    public function setLicenses($value){
        $this->licenses = $value;
    }

    public function getMaxMonthlyHours(){
        return $this->maxMonthlyHours;
    }

    public function getMaxWeeklyHours(){
        return $this->maxWeeklyHours;
    }

    public function getLicenses(){
        return $this->licenses;
    }

    /**
     * @param bool|false $percentage
     * @return float|int
     */
    public function getMonthlyLimitAccomplishment($percentage = false){
        if (!$this->maxMonthlyHours){
            return 0;
        }

        if ($percentage){
            return 100 * round($this->staff->getTotalShiftHours() / $this->maxMonthlyHours, 2);

        }
        return round($this->staff->getTotalShiftHours() / $this->maxMonthlyHours, 2);
    }

    /**
     * @var \App\Classes\Staff\Roster\ServiceTypes\Service $service
     * @return bool
     */
    public function hoursLimitCheck($service){

        $newJobDate = $service->getReportDate();

        $lastWeek  = date("Y-m-d", strtotime("- 7 days", strtotime($newJobDate)));
        $lastMonth = date("Y-m-d", strtotime("- 1 month", strtotime($newJobDate)));

        $totalShiftHours = $cumulativeMonthlyShiftHours = $cumulativeWeeklyShiftHours = 0;
        foreach ($this->staff->getShiftsList() as $shift) {

            $shiftDuration = $shift->getDuration(true);

            if (strtotime($shift->getStartDate()) > strtotime($lastWeek) &&
                strtotime($shift->getStartDate()) <= strtotime($newJobDate))
            {
                $cumulativeWeeklyShiftHours += $shiftDuration;
            }

            if (strtotime($shift->getStartDate()) > strtotime($lastMonth) &&
                strtotime($shift->getStartDate()) <= strtotime($newJobDate))
            {
                $cumulativeMonthlyShiftHours += $shiftDuration;
            }

            $totalShiftHours += $shiftDuration;
        }

        $this->staff->setTotalShiftHours($totalShiftHours);

        if ($cumulativeWeeklyShiftHours + $service->getDuration(true) > $this->maxWeeklyHours){
            return false;
        }

        if ($cumulativeMonthlyShiftHours + $service->getDuration(true) > $this->maxMonthlyHours){
            return false;
        }

        return true;

    }
}