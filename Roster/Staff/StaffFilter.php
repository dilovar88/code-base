<?php

namespace App\Classes\Staff\Roster\Staff;

use App\Classes\Staff\Roster\FlightHandler;
use App\Classes\Staff\Roster\ServiceTypes\Service;
use App\Models\Airline;
use App\Models\Airport;

class StaffFilter
{
    /* @var Staff[] $staffList */
    protected $staffList;

    /* @var Staff[] $filteredStaffList */
    protected $filteredStaffList;

    /* @var Staff[] $eligibleList */
    protected $eligibleList;

    /* @var Staff[] $priorityList1 */
    protected $priorityList1;

    /* @var Staff[] $priorityList2 */
    protected $priorityList2;

    /* @var FlightHandler $flightHandler */
    protected $flightHandler;

    // Current Airline Instance
    /* @var Airline $airline  */
    protected $airline;

    // Current Airport(Station) Instance
    /* @var Airport $station  */
    protected $station;

    protected $serviceId;

    protected $airportLicenseCheck;

    protected $airlineLicenseCheck;

    /**
     * @var Staff[] $staffList
     */
    public function __construct($staffList){

        $this->staffList = $staffList;
    }

    /**
     * @return Staff[]
     */
    public function getEligibleList()
    {
        return $this->eligibleList;
    }

    /**
     * @return Staff[]
     */
    public function getStaffList()
    {
        return $this->staffList;
    }

    public function setAirlineLicenseCheck($value){
        $this->airlineLicenseCheck = $value;
    }

    public function setAirportLicenseCheck($value){
        $this->airportLicenseCheck = $value;
    }

     /* @var FlightHandler $flightHandler */
     public function setFlight($flightHandler){

         $this->filteredStaffList = $this->eligibleList = [];

         $this->flightHandler = $flightHandler;

         $this->airline = $flightHandler->getAirline();

         $this->station = $flightHandler->getStation();
     }

    public function setFilteredStaffList($value){
        $this->filteredStaffList = $value;
    }

     /**
      * Validation 1
     * @return \App\Classes\Staff\Roster\Staff\Staff[]|array
     */
    public function skipAndPrioritizeStaff(){

        $this->priorityList1 = $this->priorityList2 = [];

        if (!$this->staffList){
            return;
        }

        foreach ($this->staffList as $i => &$each) {

            if ($this->station){
                // If Airport Not Matching => Skip Staff
                if(! ($each->getStation() && ( $each->getStation()->id == $this->station->id )))
                {
                    continue;
                }
            }

            if ($this->airline){

                // If Airline Not in the list of Staff's Service Airlines => Skip Staff
                if(! (count($each->getServiceAirlines()) && in_array($this->airline->id, $each->getServiceAirlines())))
                {
                    continue;
                }
            }

            if ($this->shiftDatePriority($each)){
                $this->priorityList1[] = $each;
            }
            else {
                $this->priorityList2[] = $each;
            }
        }

        return $this->mergePriorityLists($this->filteredStaffList);
    }

    /**
     * @var Staff[] $list
     * @return Staff[]
     */
    public function getRosteredStaffStations(){

        $stations = [];
        foreach ($this->staffList as $i => &$each) {

            if ($each->getShiftCount() > 0 && $each->getStation() && !isset($stations[$each->getStation()->id])){
                $stations[$each->getStation()->id] = $each->getStation();
            }
        }

        return $stations;
    }

    /* @var Staff[] $list */
    public function getRosteredStaff(&$list){

        if ($list && count($list)){

            foreach ($list as $i => &$each) {

                if ($each->getShiftCount() == 0){
                    unset($list[$i]);
                }
            }
        }

    }

    /**
     * @param Staff[] $eligibleStaffList
     * @param Service $service
     * @return array
     */
    public function checkConsequenceDaysLimitation($eligibleStaffList, $service){
        $list = [];

        $reportDate = strtotime($service->getReportDate());

        foreach ($eligibleStaffList as $staff) {

            $consequence = [];
            foreach ($staff->shiftDays as $i => $day) {
                if ($i == 0){
                    $consequence = [$day];
                    continue;
                }

                if (strtotime($day) == (strtotime(last($consequence)) + 86400)){
                    $consequence[] = $day;
                    continue;
                }

                $consequence = [$day];
            }

            if (!count($consequence) || count($consequence) < 6){
                $list[] = $staff;
                continue;
            }

            if ($reportDate == (strtotime(last($consequence)) + 86400)
            || $reportDate == (strtotime($consequence[0]) - 86400)){
                debug("Report: ".$service->getReportDate()." => ". implode(', ', $consequence));
                continue;
            }

            $list[] = $staff;

            /*
            if ($reportDate > (strtotime(last($consequence)) + 86400)) {
                $list[] = $staff;
                continue;
            }
            elseif ($reportDate < (strtotime($consequence[0]) - 86400)) {
                $list[] = $staff;
                continue;
            }
            */

        }

        return $list;
    }

    /**
     * @param $staffLeaves []
     * @param Staff[] $eligibleStaffList
     * @param Service $service
     * @return array
     */
    public function checkLeaveRequests($staffLeaves, $eligibleStaffList, $service){
        $list = [];

        $reportTimeStr = strtotime($service->getReportTime());
        $releaseTimeStr = strtotime($service->getReleaseTime());

        foreach ($eligibleStaffList as $item) {
            if (!isset($staffLeaves[$item->userID])){
                $list[] = $item;
                continue;
            }

            $skip = false;
            foreach ($staffLeaves[$item->userID] as $each){
                $from = strtotime($each[0]);
                $to = strtotime($each[1]);

                if (($to >= $reportTimeStr && $to <= $releaseTimeStr)
                    || ($from >= $reportTimeStr && $from <= $releaseTimeStr)
                        || ($from <= $reportTimeStr && $to >= $releaseTimeStr)){
                    $skip = true;
                    break;
                }
            }

            if (!$skip){
                $list[] = $item;
            }
        }

        return $list;
    }

    public function checkAirportLicenseRequirements($airportLicenses, $serviceDate){
        // Skip Staff That Do Not Have Required Licenses
        if ($this->airportLicenseCheck) {
            $list = [];
            foreach ($this->filteredStaffList as $staff) {
                if (!$staff->getValidation()->checkAirportLicenses($airportLicenses, $serviceDate)) {
                    continue;
                }

                $list[] = $staff;
            }

            $this->filteredStaffList = $list;
        }

        return $this->filteredStaffList;
    }

    /**
     * Validation 2
     * @var Service $service
     * @return Staff[]
     */
    public function checkStaffServicesLimitsLicenses($service){

        $this->serviceId = $service->getServiceId();

        $this->priorityList1 = $this->priorityList2 = [];

        $this->eligibleList = [];

        if (!$this->filteredStaffList && !count($this->filteredStaffList)){
            return [];
        }


        foreach ($this->filteredStaffList as $i => &$staff) {

            $servicesList = $staff->getServicesList();

            // Skip Staff Who Can Not Perform Service
            if (!count($servicesList) || !isset($servicesList[$this->airline->id])){
                continue;
            }


            // Skip Staff That Do Not Have Required Licenses
            if ($this->airlineLicenseCheck) {
                if (!$staff->getValidation()->checkLicense($service)) {
                    continue;
                }
            }

            // Function List Sample: airline_id => [ job_function_ids ]
            if (in_array($this->serviceId, $servicesList[$this->airline->id])){

//                debug($staff->getName());
                // Month And Week Limit Check
                if (!$staff->getValidation()->hoursLimitCheck($service)){
                    continue;
                }


                if ($this->shiftDatePriority($staff)){
                    $this->priorityList1[] = $staff;
                }
                else {
                    $this->priorityList2[] = $staff;
                }
            }
        }

        return $this->mergePriorityLists($this->eligibleList);
    }

    /**
     * Validation 3a. Less Hours on Top
     * @var Service $service
     */
    public function prioritiesMonthlyLimitAccomplishmentHours(&$list){

        usort($list, array($this, "compareMonthlyAccomplishment"));

        $this->eligibleList = $list;

    }

    /**
     * Validation 3b. Less Hours on Top
     * @var Service $service
     * @param bool $topDown
     * @return Staff[]
     */
    public function prioritiesTotalHours(&$list, $topDown = false){

        if ($list && count($list)) {
            if ($topDown) {
                usort($list, array($this, "compareTotalHoursTopDown"));
            } else {
                usort($list, array($this, "compareTotalHours"));
            }
        }

        $this->eligibleList = $list;
    }

    /**
     * @var Staff $staffA
     * @var Staff $staffB
     * @return int
     */
    function compareTotalHours($staffA, $staffB)
    {
        return $staffA->getTotalShiftHours() < $staffB->getTotalShiftHours();
    }

    /**
     * Validation 3a. Less Hours on Top
     * @var Service $service
     */
    public function prioritiesByNightShiftsQty(&$list){

        usort($list, array($this, "compareNightShiftsQty"));

        $this->eligibleList = $list;

    }

    /**
     * @var Staff $staffA
     * @var Staff $staffB
     * @return int
     */
    function compareNightShiftsQty($staffA, $staffB)
    {
        return $staffA->getTotalNightShifts() < $staffB->getTotalNightShifts();
    }

    public function orderByStation(&$list){

        if ($list && count($list)) {
            usort($list, array($this, "compareByStation"));
        }

        $this->eligibleList = $list;
    }

    /**
     * @var Staff $staffA
     * @var Staff $staffB
     * @return int
     */
    function compareByStation($staffA, $staffB)
    {
        return strcmp($staffA->getStation()->iata, $staffB->getStation()->iata);
    }



    /**
     * @var Staff $staffA
     * @var Staff $staffB
     * @return int
     */
    function compareTotalHoursTopDown($staffA, $staffB)
    {
        return $staffB->getTotalShiftHours() < $staffA->getTotalShiftHours();
    }

    /**
     * @var Staff $staffA
     * @var Staff $staffB
     * @return int
     */
    function compareMonthlyAccomplishment($staffA, $staffB)
    {
        return $staffA->getValidation()->getMonthlyLimitAccomplishment() < $staffB->getValidation()->getMonthlyLimitAccomplishment();
    }


    public function mergePriorityLists(&$list){
        if (count($this->priorityList1) && count($this->priorityList2)){
            $list = array_merge($this->priorityList1, $this->priorityList2);
        }
        else {
            if (count($this->priorityList1)){
                $list = $this->priorityList1;
            }
            else {
                $list = $this->priorityList2;
            }
        }

        return $list;
    }

    public function filterStaffShiftDatePriority(){

        $this->priorityList1 = $this->priorityList2 = [];

        foreach ($this->filteredStaffList as &$each) {

            if ($this->shiftDatePriority($each)){
                $this->priorityList1[] = $each;
            }
            else {
                $this->priorityList2[] = $each;
            }
        }

        return $this->mergePriorityLists($this->filteredStaffList);
    }

    /**
     * @var Staff $staff
     * @return bool
     */
    public function shiftDatePriority($staff){

        if ($staff->getCurrentShift()) {
            $shiftDate = date("Y-m-d", strtotime($staff->getCurrentShift()->getStartTime()));
            $flightDate = date("Y-m-d", strtotime($this->flightHandler->getFlightDepartureTime()));

            if (strtotime($shiftDate) == strtotime($flightDate)) {
                return true;
            }
        }

        return false;
    }


    /**
     * Validation 4.
     * @var Service $service
     */
    public function prioritiesShiftMinRestTime(&$list){

        if ($list && count($list)) {
            usort($list, array($this, "compareShiftEndTime"));
        }

        $this->eligibleList = $list;
    }

    /**
     * @var Staff $staffA
     * @var Staff $staffB
     * @return int
     */
    function compareShiftEndTime($staffA, $staffB)
    {
        if (!$staffA->getCurrentShift() && !$staffB->getCurrentShift()){
            return null;
        }

        if (!$staffA->getCurrentShift()){
            return true;
        }

        if (!$staffB->getCurrentShift()){
            return false;
        }

//        return strtotime($staffA->getCurrentShift()->getEndTime()) < strtotime($staffB->getCurrentShift()->getEndTime());
        return strtotime($staffA->getCurrentShift()->getStartTime()) >= strtotime($staffB->getCurrentShift()->getStartTime());
    }

}