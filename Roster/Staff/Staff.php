<?php

namespace App\Classes\Staff\Roster\Staff;

use App\Classes\Staff\Roster\FlightHandler;
use App\Classes\Staff\Roster\Models\StaffLicenses;
use App\Classes\Staff\Roster\Models\StaffServices;
use App\Classes\Staff\Roster\Roster;
use App\Classes\Staff\Roster\ServiceTypes\Service;
use App\Classes\Staff\Roster\Shift\Job;
use App\Classes\Staff\Roster\Shift\Shift;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class Staff extends Model
{
    protected $table = "staff_roster__staff";

    public $timestamps = true;

    public function servicesDB(){
        return $this->hasMany(StaffServices::class, "staff_roster_staff_id");
    }

    public function licensesDB(){
        return $this->hasMany(StaffLicenses::class, "staff_roster_staff_id");
    }

    public function shiftsDB(){
        return $this->hasMany(Shift::class, "staff_roster_staff_id");
    }

    // EXCLUDED FROM DB
    /* @var Roster $roster */
    protected $roster;

    /* @var Shift $currentShift */
    protected $currentShift;

    protected $serviceAirlines = [];

    // Sample: airline_id => [ service_id ]
    protected $servicesList = [];

    /* @var StaffValidation $validation */
    protected $validation;

    public $differentLocation;

    public $shiftDays = [];

    // IN DB
    protected $staffStatus;

    /* @var Shift[] $shiftsList List of Shift objects. */
    protected $shiftsList = [];

    protected $totalShiftHours;

    protected $firstShiftDate;

    protected $lastShiftDate;

    /* @var \App\Models\Airport $station */
    protected $station;

    /* @var User $user */
    protected $user;

    public $userID;


    /**
     * Staff constructor.
     * @var Roster $roster
     * @var User $user
     * @var Service[] $servicesList
     * @var null $licenses
     * @var null $differentLocation
     */
    public function __construct($roster = null, $user = null, $servicesList = null, $licenses = null){

        parent::__construct();

        $this->roster = $roster;

        $this->user = $user;

        $this->userID = $user ? $user->id : null;

        // Assigned from different location
        // Do not use for other flight rosters
        $this->station = $this->user && $this->user->location ? $this->user->location->airport : null;
        $this->differentLocation = !$this->station || $this->roster->getAirport() != $this->station->id;

        // Services List
        $this->servicesList = $servicesList;

        $this->setServiceAirlines();

        $this->validation = new StaffValidation($this, $licenses);
    }

    public function saveResult($roster)
    {
        // Save Current Roster
        $this->roster_id = $roster->id;

        $this->user_id = $this->user->id;
        $this->station_id = $this->station->id;
        $this->status = $this->staffStatus;

        $this->total_shift_hours = $this->totalShiftHours;
        $this->max_monthly_hours = $this->validation->getMaxMonthlyHours();

        $this->first_shift_date = $this->firstShiftDate;
        $this->last_shift_date = $this->lastShiftDate;

        $this->save();
    }

    public function loadFromDB($roster, $usersArray, $airportsArray, $licenses)
    {
        // Load
        $this->roster = $roster;
        $this->userID = $this->user_id;

        $this->user = isset($usersArray[$this->user_id]) ? $usersArray[$this->user_id] : null;
        // Set station
        $this->station = isset($airportsArray[$this->station_id]) ? $airportsArray[$this->station_id] : null;
        $this->differentLocation = !$this->station || $this->roster->getAirport() != $this->station->id;

        $this->staffStatus = $this->status;

        $this->totalShiftHours = $this->total_shift_hours;
        if (!$this->validation){
            $this->validation = new StaffValidation($this, $licenses);
        }
        else {
            $this->validation->setLicenses($licenses);
        }
        $this->validation->setMaxMonthlyHours($this->max_monthly_hours);
        $this->validation->setWeeklyHours();

        $this->firstShiftDate = $this->first_shift_date;
        $this->lastShiftDate = $this->last_shift_date;
    }


    /**
     * @var Service $service
     * @var FlightHandler $flightHandler
     * @var boolean $assigned
     * @return array
     */
    public function createNewShift(&$service, &$flightHandler, $staffType = null){

        $shift = new Shift($this->roster->getSettings(), $this);

        list($job, $k) = $shift->assignService($service, $flightHandler, $staffType);

        if ($this->getCurrentShift()){
            $this->currentShift->setActualRestTime($shift);
        }

        // Assign New Current Shift
        $this->currentShift = $shift;

        $this->shiftsList[] = $this->currentShift;



        return [$job, $k];
    }

    public function addShiftDate($startDate, $endDate){
        if (!in_array($startDate, $this->shiftDays)){
            $this->shiftDays[] = $startDate;
        }

        if (!in_array($endDate, $this->shiftDays)){
            $this->shiftDays[] = $endDate;
        }

        sort($this->shiftDays);
    }

    public function getName(){
        return getUserName($this->user);
    }

    public function setTotalShiftHours($hours){
        $this->totalShiftHours = $hours;
    }

    public function getTotalShiftHours(){
        return $this->totalShiftHours;
    }

    public function getTotalNightShifts(){
        $total = 0;
        foreach ($this->shiftsList as $item) {
            if ($item->getShiftType() == NIGHT_SHIFT){
                $total++;
            }
        }

        return $total;
    }

    public function getShiftsList(){
        return $this->shiftsList;
    }

    public function setShiftsList($value){
        $this->shiftsList = $value;
    }

    public function addShiftToShiftsList($value){
        $this->shiftsList[] = $value;
    }

    /* @var \App\Classes\Staff\Roster\Shift\Shift $shift */
    public function setFirstLastShiftDates($shift){
        if (!$this->firstShiftDate){
            $this->firstShiftDate = $shift->getStartDate();
        }
        else {
            if (strtotime($shift->getStartDate()) < strtotime($this->firstShiftDate)){
                $this->firstShiftDate = $shift->getStartDate();
            }
        }

        if (!$this->lastShiftDate){
            $this->lastShiftDate = $shift->getEndDate();
        }
        else {
            if (strtotime($shift->getEndDate()) > strtotime($this->lastShiftDate)){
                $this->lastShiftDate = $shift->getEndDate();
            }
        }
    }

    public function getValidation(){
        return $this->validation;
    }

    public function getUser(){
        return $this->user;
    }

    public function getShiftCount(){
        return count($this->getShiftsList());
    }

    public function getStation(){
        return $this->station;
    }

    public function getServicesList(){
        return $this->servicesList;
    }

    public function getCurrentShift(){
        return $this->currentShift;
    }

    public function setServicesList($value){
        $this->servicesList = $value;
    }

    public function setServiceAirlines(){
        if ($this->servicesList && count($this->servicesList)){
            $this->serviceAirlines = array_keys($this->servicesList);
        }
    }

    public function getServiceAirlines(){
        return $this->serviceAirlines;
    }

    public function printOutDetails(){

        $user = $this->getUser();
        echo "<tr>" .
                "<td>{$user->id}</td>".
                "<td>{$user->last_name} {$user->first_name}</td>".
                "<td>{$this->validation->getMonthlyLimitAccomplishment(true)}%</td>".
                "<td>".count($this->shiftsList)."</td>".
                "<td>{$this->getTotalShiftHours()}</td>".
                "<td>{$this->validation->getMaxWeeklyHours()}</td>".
                "<td>{$this->validation->getMaxMonthlyHours()}</th>".
            "</tr>";

        foreach ($this->shiftsList as $i => $shift) {
            echo "<tr><td colspan='10'>";
            echo ($i + 1).") Shift Start: ". $shift->getStartTime()." | End: ". $shift->getEndTime()." | MAX HRS: ". $shift->getValidation()->getMaxHours().
                " | Duration: ". $shift->getDuration(true). " | Rest Time: ". $shift->getActualRestTime(). "<br/>";

            foreach ($shift->getJobsList() as $job) {
                echo $job->getServiceName()." Report: ".$job->getReportTime()."| Release: ".$job->getReleaseTime().
                    "| Duration: ".$job->getDuration(true)."| Break Time: ".$job->getBreakTime(). "<br/>";
            }
            echo "</td></tr>";
        }
        echo "<br/>";
    }
}