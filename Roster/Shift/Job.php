<?php

namespace App\Classes\Staff\Roster\Shift;

use App\Classes\Staff\Roster\FlightHandler;
use App\Classes\Staff\Roster\ServiceTypes\Service;
use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
    protected $table = "staff_roster__jobs";

    public $timestamps = true;

    public function shiftDB(){
        return $this->belongsTo(Shift::class, "shift_id");
    }

    public function serviceDB(){
        return $this->belongsTo(Service::class, "staff_roster_service_id");
    }

    public function flightHandlerDB(){
        return $this->belongsTo(FlightHandler::class, "flight_handler_id");
    }

    // EXCLUDED FROM DB

    protected $serviceName;

    // IN DB

    /* @var Service $service  */
    protected $service;

    /* @var Shift $shift */
    protected $shift;

    protected $reportTime;

    protected $releaseTime;

    protected $jobDuration;

    protected $breakTime;

    /* @var FlightHandler $flightHandler  */
    protected $flightHandler;

//    public $assignedManually;

    public $staffType;

    protected $uid;

    /**
     * @var Shift $shift
     * @var Service $service
     * @var $flightHandler
     * @var boolean $assignedManually
     */
    public function __construct($shift = null, $service = null, $flightHandler = null, $staffType = null){

        parent::__construct();

        $this->uid = rand(1000000, 9999999);

        $this->shift = $shift;

        $this->service = $service;

        $this->reportTime = $service ? $service->getReportTime() : null;

        $this->releaseTime = $service ? $service->getReleaseTime() : null;

        $this->serviceName = $service ? $service->getServiceName() : null;

        $this->flightHandler = $flightHandler;

//        $this->assignedManually = $assignedManually;

        $this->staffType = $staffType;

        $this->setDuration();
    }

    /**
     * @return int
     */
    public function getUid()
    {
        return $this->uid;
    }

    public function saveResult()
    {
        // Save Current Instance
        $this->shift_id = $this->shift->id;
        $this->staff_roster_service_id = $this->service->id;
        $this->flight_handler_id = $this->flightHandler->id;
        $this->report_time = $this->reportTime;
        $this->release_time = $this->releaseTime;
        $this->duration = $this->jobDuration;
        $this->break_time = round($this->breakTime, 6);
//        $this->assigned = $this->assignedManually;
        $this->staff_type = $this->staffType;
        $this->save();
    }

    public function loadFromDB($shift, $flightHandler, $service)
    {
        // Save Current Instance
        $this->shift = $shift;
        $this->flightHandler = $flightHandler; //$flightHandlersArray[$this->flight_handler_id];
        $this->service = $service; // $servicesArray[$this->staff_roster_service_id];
        $this->serviceName = $service->getServiceName();

        $this->reportTime = $this->report_time;
        $this->releaseTime = $this->release_time;
        $this->jobDuration = $this->duration;
        $this->breakTime = $this->break_time;
//        $this->assignedManually = $this->assigned;
        $this->staffType = $this->staff_type;
    }

    /* @var Job $newJob */
    public function setBreakTime($breakTime){

        $this->breakTime = $breakTime;
    }

    /* @var Shift $shift */
    public function setBreakTimeFromShift($shift){

        $this->breakTime = Calculate_Duration($this->getReleaseTime(), $shift->getStartTime(), null, 16);
    }

    public function getBreakTime(){
        return $this->breakTime;
    }

    public function getShift(){
        return $this->shift;
    }

    public function getDuration($hour = false){
        if ($hour){
            return round($this->jobDuration / 60, 2);
        }

        return $this->jobDuration;
    }

    public function getService(){
        return $this->service;
    }

    public function getReportTime(){
        return $this->reportTime;
    }

    public function getReleaseTime()
    {
        return $this->releaseTime;
    }

    public function getReportTimeTimezone(){
        if ($this->shift->timing == LOCAL){
            return utcToLocal($this->shift->settings->getRoster()->airportTimezones, $this->flightHandler->getStation()->id, $this->reportTime);
        }

        return $this->reportTime;
    }

    public function getReleaseTimeTimezone(){
        if ($this->shift->timing == LOCAL){
            return utcToLocal($this->shift->settings->getRoster()->airportTimezones, $this->flightHandler->getStation()->id, $this->releaseTime);
        }

        return $this->releaseTime;
    }

    public function getServiceName(){
        if ($this->serviceName){
            return $this->serviceName;
        }
        else {
            return $this->service->getServiceName();
        }
    }

    public function getFlightHandler(){
        return $this->flightHandler;
    }

    public function getFlightID(){

        return $this->flightHandler->getFlight()->id;
    }

    public function getFlightNumber(){

        $fn = $this->flightHandler && $this->flightHandler->getFlightNumber() ? $this->flightHandler->getFlightNumber() : null;

        $flightNumber = $fn && $fn->airline ? $fn->airline->iata."-" : "";

        $flightNumber .= $fn ? $fn->flight_number : "";

        return $flightNumber;
    }

    public function getSector(){

        $fn = $this->flightHandler && $this->flightHandler->getFlightNumber() ? $this->flightHandler->getFlightNumber() : null;

        $sector = $fn && $fn->departureAirport ? $fn->departureAirport->iata."-" : "";

        $sector .= $fn && $fn->arrivalAirport ? $fn->arrivalAirport->iata : "";

        return $sector;
    }

    public function getDepartureAirportID(){

        $fn = $this->flightHandler && $this->flightHandler->getFlightNumber() ? $this->flightHandler->getFlightNumber() : null;

        if (!$fn){
            return null;
        }

        return $fn->departure_airport_id;
    }

    public function getArrivalAirportID(){

        $fn = $this->flightHandler && $this->flightHandler->getFlightNumber() ? $this->flightHandler->getFlightNumber() : null;

        if (!$fn){
            return null;
        }

        return $fn->departure_airport_id;
    }

    public function setDuration(){
        $this->jobDuration = round(60 * Calculate_Duration($this->reportTime, $this->releaseTime, null, 16), 2);
    }

}