<?php

namespace App\Classes\Staff\Roster\Shift;

use App\Classes\Staff\Roster\Helpers;
use App\Classes\Staff\Roster\Settings\RosterSettings;
use App\Classes\Staff\Roster\ServiceTypes\Service;

class ShiftValidation
{
    /* @var Shift */
    protected $shift;

    // Max Allowed Shift Time
    protected $maxHours =  10;

    // Min Allowed Shift Time
    protected $minHours = 3;

    protected $minRestTime = 10.5;

    /**
     * @var RosterSettings $settings
     * @var Shift $shift
     */
    public function __construct($settings, $shift){

        $this->shift = $shift;

        if ($settings){

            if ($settings->getShiftMaxHours()){
                $this->maxHours = $settings->getShiftMaxHours();
            }
            if ($settings->getShiftMinHours()){
                $this->minHours = $settings->getShiftMinHours();
            }
            if ($settings->getShiftMinRestTime()){
                $this->minRestTime = $settings->getShiftMinRestTime();
            }
        }
    }

    public function setMaxHours($hours){
        $this->maxHours = $hours;
    }

    public function setMinHours($hours){
        $this->minHours = $hours;
    }

    public function setMinRestTime($hours){
        $this->minRestTime = $hours;
    }

    public function getMaxHours(){
        return $this->maxHours;
    }

    public function getMinHours(){
        return $this->minHours;
    }

    public function getMinRestTime(){
        return $this->minRestTime;
    }


    public function eligibleAsNewShift($service, $skipMinHoursCheck = false){

        if (!$skipMinHoursCheck){
            if ($this->shift->getDuration(true) < $this->shift->getValidation()->getMinHours()){
            //debug("SHIFT DURATION: ". $this->shift->getDuration(true). " < SHIFT MIN HOURS: ".  $this->shift->getValidation()->getMinHours());
                return false;
            }
        }

        $availableTimeAfterRest = Add_Minutes_To_DateTime($this->shift->getEndTime(), $this->shift->getValidation()->getMinRestTime(), true);

//        debug($availableTimeAfterRest."/".$service->getReportTime());

        if (strtotime($availableTimeAfterRest) <= strtotime($service->getReportTime())){
            return true;
        }

        return false;
    }

    /**
     * @var Service $service
     * @return bool
     */
    public function checkMaxHoursLimit($service){

        // Time in between (in mins)
        $timeInBetween = 60 * Calculate_Duration($this->shift->getEndTime(), $service->getReportTime(), null, 16);

        return ($this->shift->getDuration() + $timeInBetween + $service->getDuration()) + 60  >= ($this->maxHours * 60);
    }

    /**
     * @var Service $service
     * @return bool
     */
    public function validateService($service, $sameFlightID = null){

        // If new JOB report Time And Release Time Does Not Conflicts With Shift's Timings
        if (Helpers::jobAndShiftTimingConflicts($this->shift, $service)){
            return false;
        }

        // If new JOB Duration + Shift's Current Duration > Shift's MAX HOURS Limit
        if ($this->checkMaxHoursLimit($service)){
            return false;
        }

        // Pick same flight only if set
        if ($sameFlightID){
            return $this->shift->getCurrentJob()->getFlightID() == $sameFlightID;
        }

        return true;
    }

    /**
     * @var Service $service
     * @return bool
     */
    public function validateAssignedService($service, $sameFlightID = null){

        // If new JOB Duration + Shift's Current Duration > Shift's MAX HOURS Limit
        if ($this->checkMaxHoursLimit($service)){
            return false;
        }

        // Pick same flight only if set
        if ($sameFlightID){
            return $this->shift->getCurrentJob()->getFlightID() == $sameFlightID;
        }

        return true;
    }
}