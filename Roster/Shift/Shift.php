<?php

namespace App\Classes\Staff\Roster\Shift;

use App\Classes\Staff\Roster\Constants;
use App\Classes\Staff\Roster\FlightHandler;
use App\Classes\Staff\Roster\ServiceTypes\Service;
use App\Classes\Staff\Roster\Settings\RosterSettings;
use App\Classes\Staff\Roster\Staff\Staff;
use Illuminate\Database\Eloquent\Model;

class Shift extends Model
{
    protected $table = "staff_roster__shifts";

    public $timestamps = true;

    public function jobsDB(){
        return $this->hasMany(Job::class, "shift_id");
    }

    public function staffDB(){
        return $this->belongsTo(Staff::class, "staff_roster_staff_id");
    }

    // EXCLUDED FROM DB
    /* @var Job $firstJob */
    protected $firstJob;

    /* @var Job $currentJob */
    protected $currentJob;

    protected $validation;

    public $timing = UTC;

    public $settings;

    // GMT + 3 HRS
    const LOCAL = 3 * 60 * 60;

    // IN DB

    // Day / Night Shift
    protected $shiftType;

    /* @var Staff $staff */
    protected $staff;

    // Shift Start Time
    protected $startTime;

    // Shift End Time
    protected $endTime;

    protected $actualRestTime;

    // Current Shift Duration
    protected $shiftDuration;

    /* @var Job[] $jobsList */
    protected $jobsList = [];

    /**
     * @var RosterSettings $settings
     * @var Staff $staff
     */
    public function __construct($settings = null, $staff = null){

        parent::__construct();

        $this->settings = $settings;

        $this->timing = $settings && $settings->getRoster() ? $settings->getRoster()->timing : UTC;

        $this->staff = $staff;

        $this->shiftType = DAY_SHIFT;

        $this->validation = new ShiftValidation($settings, $this);
    }

    public function saveResult()
    {
        // Save Current Instance
        $this->staff_roster_staff_id = $this->staff->id;
        $this->type = $this->shiftType;
        $this->start_time = $this->startTime;
        $this->end_time = $this->endTime;
        $this->actual_rest_time = $this->actualRestTime;
        $this->duration = $this->shiftDuration;
        $this->save();
    }


    public function loadFromDB($settings, $staff)
    {
        // Load
        $this->settings = $settings;
        $this->validation = new ShiftValidation($settings, $this);

        $this->staff = $staff;
        $this->shiftType =$this->type;
        $this->startTime = $this->start_time;
        $this->endTime = $this->end_time;
        $this->actualRestTime = $this->actual_rest_time;
        $this->shiftDuration = $this->duration;
    }


    /**
     * @var Service $service
     * @var FlightHandler $flightHandler
     * @var boolean $assigned
     * @return array
     */
    public function assignService(&$service, &$flightHandler, $staffType = null){


        $job = new Job($this, $service, $flightHandler, $staffType);

        if (count($this->jobsList)) {
            $jobStart = strtotime($job->getReportTime());
            $j = $found = false;

            foreach ($this->jobsList as $i => $each) {
                if ($jobStart < strtotime($each->getReportTime())) {
                    $j = $i;
                    $found = true;
                    break;
                }
            }

            if ($found){
                array_splice($this->jobsList, $j, 0, [$job]);
            }
            else {
                $j = count($this->jobsList);

                $this->jobsList[] = $job;
            }
        }
        else {
            $j = count($this->jobsList);

            $this->jobsList[] = $job;
        }

        $this->setType($job->getReportTime());

        $this->setNewJobTimings($job);

        // Shift days
        $this->staff->addShiftDate($this->getStartDate(), $this->getEndDate());

        $this->staff->setFirstLastShiftDates($this);

        if ($this->getCurrentJob()) {
            if ($j > 0){
                $breakTime = Calculate_Duration($this->jobsList[$j - 1]->getReleaseTime(), $job->getReportTime(), null, 16);
//                debug($breakTime);
                if ($breakTime > 0){
                    // Check break time
                    $this->jobsList[$j-1]->setBreakTime($breakTime);
                }
                else {
                    $this->jobsList[$j-1]->setBreakTime(0);
                }
            }
        }

        // Replace Current Job
        $this->currentJob = $job;

        if (!$this->firstJob){
            $this->firstJob = $job;
        }

        return [$this->currentJob, $j];
    }



    /* @var Shift $newShift */
    public function setActualRestTime($newShift){

        $this->actualRestTime = Calculate_Duration($this->getEndTime(), $newShift->getStartTime(), null, 16);

        $this->currentJob->setBreakTimeFromShift($newShift);
    }

    /* @return Staff */
    public function getStaff(){
        return $this->staff;
    }

    public function getType(){
        return $this->shiftType;
    }

    /* @return Job */
    public function getCurrentJob(){
        return $this->currentJob;
    }

    public function getValidation(){
        return $this->validation;
    }

    /* @return Job[] */
    public function getJobsList(){
        return $this->jobsList;
    }

    public function setJobsList($value){
        $this->jobsList = $value;
    }

    public function addJobToJobsList($value){
        $this->jobsList[] = $value;
    }

    public function getActualRestTime(){
        return $this->actualRestTime;
    }


    public function getDuration($hour = false){
        if ($hour)
            return round($this->shiftDuration / 60, 4);

        return $this->shiftDuration;
    }

    /**
     * @return mixed
     */
    public function getStartTime()
    {
        return $this->startTime;
    }

    /**
     * @return mixed
     */
    public function getEndTime()
    {
        return $this->endTime;
    }

    public function getStartTimeTimezone(){
        if ($this->timing == LOCAL){
            return utcToLocal($this->settings->getRoster()->airportTimezones,$this->firstJob->getFlightHandler()->getStation()->id, $this->startTime);
        }

        return $this->startTime;
    }

    public function getEndTimeTimezone(){
        if ($this->timing == LOCAL){
            return utcToLocal($this->settings->getRoster()->airportTimezones,$this->currentJob->getFlightHandler()->getStation()->id, $this->endTime);
        }

        return $this->endTime;
    }

    public function getStartDate(){
        return date("Y-m-d", strtotime($this->startTime));
    }

    public function getEndDate(){
        return date("Y-m-d", strtotime($this->endTime));
    }


    /* @var Job $job  */
    public function setNewJobTimings($job){
        if (!$this->startTime
            || ($this->startTime && (strtotime($job->getReportTime()) < strtotime($this->startTime)))){
            $this->startTime = $job->getReportTime();
        }

        if (!$this->endTime
            || ($this->endTime && (strtotime($job->getReleaseTime()) > strtotime($this->endTime)))){
            $this->endTime = $job->getReleaseTime();
        }

        $this->shiftDuration =  round(60 * Calculate_Duration($this->startTime, $this->endTime, null, 16), 4);
    }

    /**
     * @return string
     */
    public function getShiftType()
    {
        return $this->shiftType;
    }

    public function setType($reportTime){

        $hourMin = strtotime(date('H:i', strtotime($reportTime) + self::LOCAL));

        if (($hourMin >= strtotime('00:00') && $hourMin <= strtotime('07:00')) ||
            ($hourMin >= strtotime('20:00') && $hourMin <= strtotime('23:59')) ) {

            $this->shiftType = Constants::NIGHT_SHIFT;

            $this->getValidation()->setMinRestTime(11);
        }

    }

}