<?php
/**
 * Created by PhpStorm.
 */

namespace App\Classes\Staff\Roster;

use App\Classes\Staff\Roster\ManuallyRostered\ManualRoster;
use App\Classes\Staff\Roster\ManuallyRostered\ManualRosterFlight;
use App\Classes\Staff\Roster\ManuallyRostered\ManualRosterService;
use App\Classes\Staff\Roster\ServiceTypes\Service;
use App\Classes\Staff\Roster\Settings\RosterSettings;
use App\Classes\Staff\Roster\Settings\RosterSettingsAssignedFlight;
use App\Classes\Staff\Roster\Settings\RosterSettingsAssignedService;
use App\Classes\Staff\Roster\Staff\Staff;
use App\Classes\Staff\Roster\Staff\StaffFilter;
use App\Models\AirlineService;
use App\Models\Airport;
use App\Models\Flight;
use App\Models\FlightStaff;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class Roster extends Model
{
    protected $table = "staff_roster";

    public $timestamps = true;

    public function airport(){
        return $this->belongsTo(Airport::class);
    }

    public function flightHandlersDB(){
        return $this->hasMany("App\\Classes\\Staff\\Roster\\FlightHandler", "roster_id");
    }

    public function airlinesDB(){
        return $this->hasMany("App\\Classes\\Staff\\Roster\\Models\\Airlines", "roster_id");
    }

    public function staffDB(){
        return $this->hasMany("App\\Classes\\Staff\\Roster\\Staff\\Staff", "roster_id");
    }

    public function settingsDB(){
        return $this->hasOne("App\\Classes\\Staff\\Roster\\Settings\\RosterSettings", "roster_id");
    }

    // EXCLUDE FROM DB
    /* @var RosterOperations $operations*/
    protected $operations;

    protected $processedFlights = [];

    /* @var $users User[] */
    protected $users;

    protected $usersIds;

    // For Assigned
//    protected $usersAssigned;
//    protected $staffListAssigned;
//    protected $staffServicesAssigned;
//    protected $usersIdsAssigned;
    // End

    protected $flightIds;

    protected $handlingStations;

    /* @var Staff[] $staffList */
    protected $staffList;

    /* @var Staff[] $staffList */
    protected $filteredStaffList;

    /* @var Staff[] $eligibleStaffList */
    protected $eligibleStaffList;

    protected $servicesList;

    protected $servicesByAirlineAndAirports;

    /* @var $staffFilter StaffFilter */
    protected $staffFilter;

    protected $manualRoster;

    protected $type;

    public $timing;

    public $airportTimezones;

    public $assignedStaff;

    // IN DB -------

    protected $fromDate;

    protected $toDate;

    protected $airport;

    // ROSTER AIRLINES TABLE
    protected $airlines;

    // AIRLINE SERVICE LICENSES TABLE
    protected $airportLicenses;

    // AIRLINE SERVICE LICENSES TABLE
    protected $airlineLicenses;

    // STAFF SERVICES TABLE
    protected $staffServices;

    // STAFF LICENSES TABLE
    protected $staffLicenses;

    // STAFF LEAVES
    protected $staffLeaves;

    /* @var RosterSettingsAssignedFlight[] $assignedFlights */
    protected $assignedFlights = [];

    protected $assignedFlightIds = [];

    protected $manuallyRosteredFlights = [];

    protected $manuallyRosteredFlightIds = [];

    // ---- FLIGHT HANDLER TABLE
        /* @var Flight[] $flights */
        protected $flights = [];

        /* @var FlightHandler[] $flightHandlers */
        protected $flightHandlers = [];
    // ----- END

    /* @var RosterSettings $settings */
    protected $settings;


    /* @var Helpers*/
    public $helpers;

    /* @var RosterSettings $settings */
    public function init($parameters = null, $settings = null){

        $this->operations = new RosterOperations($this);

        $this->helpers = new Helpers();

        if ($parameters && count($parameters)) {

            $this->type = $parameters['type'];

            $this->timing = $parameters['timing'];

            $this->fromDate = $parameters['from'];

            $this->toDate = $parameters['to'];

            $this->airport = $parameters['airport']; // is_array($airports) ? $airports : [ $airports ];

            $this->airlines = $parameters['airlines'];
        }

        if ($settings) {
            $this->settings = $settings;
            $this->settings->setRoster($this);
        }

        $this->airportTimezones = Airport::whereNotNull("timezone")
                                        ->pluck("timezone", "id")
                                        ->all();

        $this->manualRoster = new ManualRoster($this);
    }

    public function uploadSettings($settings){
        $this->settings = $settings;
    }

    /**
     * @return mixed
     */
    public function getStaffLeaves()
    {
        return $this->staffLeaves;
    }


    /**
     * @return ManualRoster
     */
    public function getManualRoster()
    {
        return $this->manualRoster;
    }

    public function getSettings(){
        return $this->settings;
    }

    public function setSettings($value){
        $this->settings = $value;
    }

    public function getOperations(){
        return $this->operations;
    }

    public function initialSetup(){

        $this->servicesList = Helpers::getServicesList();

        // Set All Handling Stations
        $this->handlingStations = Airport::getHandlingStationsIdAndObject();

        $this->servicesByAirlineAndAirports = AirlineService::getServicesByAirlineAndAirport($this->airlines, $this->airport);

        // Get All The Flights In This Period
        $this->flights = Flight::handlingFlightsRange($this->fromDate, $this->toDate, $this->airlines, $this->airport, null, [
            'off_order_by_airline_id' => TRUE
        ]);

        // Set Flight Ids
        $this->setFlightIds();

        // Get All Staff By Airports
        $this->users = User::getUsersByLocation($this->airport);

        // Get All User Ids as an Array
        $this->usersIds = Helpers::getUserIdsArray($this->users);

        // Get Functions By Users
        $this->staffServices = Helpers::getServicesByStaff($this->usersIds);

        $this->airportLicenses = Helpers::getAirportLicenseRequirements();

        $this->airlineLicenses = Helpers::getAirlineLicenseRequirements();

        $this->staffLicenses = Helpers::getAirlineLicensesByStaff($this->usersIds);

        $this->staffLeaves = Helpers::getStaffLeaveRequests($this->usersIds, $this->fromDate, $this->toDate);

        // Initiate Staff Object for each User
        $this->fillStaffListForUsers();

        $this->staffFilter = new StaffFilter($this->staffList);
    }

    public function fillStaffListForUsers(){
        foreach ($this->users as $user) {

            if (isset($this->staffServices[$user->id])){

                $licensesByStaff = isset($this->staffLicenses[$user->id]) ? $this->staffLicenses[$user->id] : null;

                $this->staffList[] = new Staff($this, $user, $this->staffServices[$user->id], $licensesByStaff);
            }
        }
    }

    public function setupManuallyRosteredStaff(){

        $this->assignedStaff = false;

        $userIDs = FlightStaff::join("flights", "flights.id", "=", "flights__staff.flight_id")
                                ->whereIn("flight_id", $this->flightIds)
                                ->whereNull("flights.deleted_at")
                                ->whereNull("flights.cancelled_at")
                                ->whereNull("flights__staff.deleted_at")
                                ->groupBy("flights__staff.user_id")
                                ->pluck("flights__staff.user_id")
                                ->all();

        $remainingUserIDs = array_diff($userIDs, $this->usersIds);

        // Get All Staff By Airports
        $remainingUsers = User::getUsersByIDs($remainingUserIDs);

        // Get All User Ids as an Array
//        $usersIdsAssigned = Helpers::getUserIdsArray($usersAssigned);

        // Get Functions By Users
        $staffServicesAssigned = Helpers::getServicesByStaff($remainingUserIDs);

        foreach ($remainingUsers as $user) {
            if (isset($staffServicesAssigned[$user->id]) && $staffServicesAssigned[$user->id]){
                $this->users[] = $user;
                $this->usersIds[] = $user->id;
                $this->staffList[] = new Staff($this, $user, $staffServicesAssigned[$user->id]);
            }
        }

        // $serviceAndStaff[$serviceId] = $userIDs
        // $flightsData[$flightId] = $serviceAndStaff;
        if ($this->flightIds && count($this->flightIds)){
            foreach ($this->flightIds as $fltID) {
                $flightsStaff = FlightStaff::where("flight_id", $fltID)
                                            ->whereNull("deleted_at")
                                            ->get();

                if ($flightsStaff->count()) {

                    $serviceAndStaff = [];
                    foreach ($flightsStaff as $each) {
                        if (!isset($serviceAndStaff[$each->service_id])){
                            $serviceAndStaff[$each->service_id] = [];
                        }

                        $serviceAndStaff[$each->service_id][] = $each->user_id;
                    }

                    //$this->manuallyRosteredFlights[] =
                    $this->manualRoster->addFlight($fltID, $serviceAndStaff);
                }
            }
        }
    }

    public function setupAssignedVariables($flightsData, $userIDs){

        $this->assignedStaff = true;

        $remainingUserIDs = array_diff($userIDs, $this->usersIds);

        // Get All Staff By Airports
        $remainingUsers = User::getUsersByIDs($remainingUserIDs);

        // Get All User Ids as an Array
        //$usersIdsAssigned = Helpers::getUserIdsArray($remainingUsers);

        // Get Functions By Users
        $staffServicesAssigned = Helpers::getServicesByStaff($remainingUserIDs);

        foreach ($remainingUsers as $user) {

            if (isset($staffServicesAssigned[$user->id])){

                $this->users[] = $user;
                $this->usersIds[] = $user->id;
                $this->staffList[] = new Staff($this, $user, $staffServicesAssigned[$user->id]);
            }
        }

//        debug($flightsData);
        foreach ($flightsData as $flightId => $servicesAndStaffList) {
            // Add Services And Staff
            $this->settings->addAssignedFlight($flightId, $servicesAndStaffList);
        }
    }

    public function setFlightIds(){
        foreach ($this->flights as $each) {
            $this->flightIds[] = $each->id;
        }
    }

    public function getStaffObjectByUserId($userId){

        foreach ($this->staffList as $staff) {

            if ($staff->getUser()->id == $userId){

                return $staff;
            }
        }

        return null;
    }

    public function prepareAssignedFlights(){
        $flights = $this->getSettings()->getAssignedFlights();
        if ($flights && count($flights)){

            foreach ($flights as $flight) {
                // Check if assigned flight belongs to selected period flights
                $flightId = $flight->getFlightHandler()->getFlight()->id;

                if (in_array($flightId, $this->flightIds)){

                    $this->assignedFlights[] = $flight;

                    $this->assignedFlightIds[] = $flightId;
                }
            }

        }
    }

    public function prepareManuallyRosteredFlights(){
        $flights = $this->getManualRoster()->getManuallyRosteredFlights();

        if ($flights && count($flights)){

            foreach ($flights as $flight) {
                // Check if assigned flight belongs to selected period flights
                $flightId = $flight->getFlightHandler()->getFlight()->id;

                if (in_array($flightId, $this->flightIds)){
                    $this->manuallyRosteredFlights[] = $flight;

                    $this->manuallyRosteredFlightIds[] = $flightId;
                }
            }

        }
    }

    public function start($method = 2){
        // Get Assigned Flights
        $this->prepareAssignedFlights();

        $this->prepareManuallyRosteredFlights();

        // Airport License Check
        $this->staffFilter->setAirportLicenseCheck($this->settings->getAirportLicenseCheck());

        // Airline License Check
        $this->staffFilter->setAirlineLicenseCheck($this->settings->getAirlineLicenseCheck());

        foreach ($this->manuallyRosteredFlights as $i => $flight) {
            $this->fillManualFlight($flight);
        }

        // Loop Through Assigned Flights
        foreach ($this->assignedFlights as $i => $assignedFlight) {
            $this->fillAssignedFlight($assignedFlight);
        }

        $this->orderByServiceStartTime();

        // Loop Through IF AUTO-picked
//        debug("TYPE:" . $this->type);
        if ($this->type == AUTO_ROSTER_TYPE) {
            foreach ($this->flights as $i => $flight) {
                $this->fillRegularFlight($flight, $method);
            }
        }

        $total = 0;
        if ($this->staffList) {
            foreach ($this->staffList as $staff) {
                $total += $staff->getTotalShiftHours();
            }
        }
        debug("TOTAL: ". $total);

        $this->staffFilter->getRosteredStaff($this->staffList);

        $this->staffFilter->prioritiesTotalHours($this->staffList, true);

        $this->staffFilter->orderByStation($this->staffList);

        // $this->echoStaffJobDetails();
        $this->setCharts();

        $this->setStaffTimeline();
    }

    protected $staffCharts;
    protected $staffRosteredCounter;

    /**
     * @return mixed
     */
    public function getStaffCharts()
    {
        return $this->staffCharts;
    }

    /**
     * @return mixed
     */
    public function getStaffRosteredCounter()
    {
        return $this->staffRosteredCounter;
    }

    function setStaffTimeline()
    {
        $json = $this->staffRosteredCounter = [];

        if (!$this->getStaffList()){
            return;
        }

        $reportDate = new \DateTime($this->fromDate);
        $releaseDate = new \DateTime($this->toDate);

        $json[] = $this->getTiming("SHIFT", "PERIOD", $reportDate, $releaseDate);
//        $json[] = $this->getTiming("SHIFT", "END", $releaseDate, $releaseDate);

        $n = 0;
        foreach ($this->getStaffList() as $staff) {
            $staffName = $staff->getName();

            foreach ($staff->getShiftsList() as $j => $shift) {
                // $shiftStartDate = $shift->getStartDate();

                if (!in_array($staffName, $this->staffRosteredCounter)){
                    $this->staffRosteredCounter[] = $staffName;
                    $n++;
                }

                foreach ($shift->getJobsList() as $k => $job) {
                    $fn = $job->getFlightNumber()."/".date("d", strtotime($job->getFlightHandler()->getFlightDepartureDate()));
                    $sector = $job->getFlightHandler()->getSector();
                    $time = $job->getFlightHandler()->getServicedTime();

                    $serviceName = $job->getServiceName()." ".$fn." ".$sector." ".$time;

                    $reportDate = new \DateTime($job->getReportTimeTimezone());
                    $releaseDate = new \DateTime($job->getReleaseTimeTimezone());

                    /*
                    $duration = $releaseDate->diff($reportDate);
                    $durationMinutes = $duration->h * 60 + $duration->i;

                    $flt['content'] = "<div id='job-" . $job->getUid() . "' class='row fw-flight-item'>" .
                        "<div class='col-sm-12 flightStandard'>" .
                        "<span class='flight_number_box_left'></span>" .
                        "<span class='flight_number_box_center'>" . $serviceName ."<br/>".
                        "<span class='flight_number_box_center_row_2'></span>".
                        "</span>" .
                        "<span class='flight_number_box_right'></span>" .
                        "</div>" .
                        "</div>";
                    */
                    $json[] = $this->getTiming( $n.". ".$staffName, $serviceName, $reportDate, $releaseDate);
                }
            }
        }

        if (count($json) > 1){
            $this->staffCharts = $json;
            $this->staffRosteredCounter[] = "Init";
        }

        $this->staffRosteredCounter = count($this->staffRosteredCounter);
    }

    function getTiming($group, $className, $reportDate, $releaseDate){
        return [
            "start_date" => [
                $reportDate->format("Y"),
                (int)$reportDate->format("n") - 1,
                $reportDate->format("j"),
                $reportDate->format("G"),
                (int)$reportDate->format("i"),
                (int)$reportDate->format("s")
            ],
            "end_date" => [
                $releaseDate->format("Y"),
                (int)$releaseDate->format("n") - 1,
                $releaseDate->format("j"),
                $releaseDate->format("G"),
                (int)$releaseDate->format("i"),
                (int)$releaseDate->format("s")
            ],
            "group"     => $group,
            "className" => $className,
            "content"   => $className,
        ];
    }

    function setStaffCharts(){
        $data = [];
        foreach ($this->getStaffList() as $staff) {
            $staffName = $staff->getName();
            foreach ($staff->getShiftsList() as $j => $shift) {
                $shiftStartDate = $shift->getStartDate();

                foreach ($shift->getJobsList() as $k => $job) {
                    if (!isset($data[$shiftStartDate])){
                        $data[$shiftStartDate]= [
                            "name"  => "SLA",
                            "data"  => []
                        ];
                    }

                    $report = 1000 * strtotime($job->getReportTime());
                    $release = 1000 * strtotime($job->getReleaseTime());

                    $data[$shiftStartDate]["data"][] = [
                        "x"         => $staffName,
                        "y"         => [
                            $report,
                            $release
                        ],
//                        "fillColor" => '#008FFB'
                    ];
                }
            }
        }

        $this->staffCharts = $data;

    }

    function setStaffCharts2(){
        $data = [];
        foreach ($this->getStaffList() as $staff) {
            $staffName = $staff->getName();
            foreach ($staff->getShiftsList() as $j => $shift) {
                $shiftStartDate = $shift->getStartDate();

                foreach ($shift->getJobsList() as $k => $job) {
                    $serviceName = $job->getServiceName();
                    if (!isset($data[$shiftStartDate][$serviceName])){
                        $data[$shiftStartDate][$serviceName] = [
                            "name"  => $serviceName,
                            "data"  => []
                        ];
                    }

                    $report = 1000 * strtotime($job->getReportTime());
                    $release = 1000 * strtotime($job->getReleaseTime());

                    $data[$shiftStartDate][$serviceName]["data"][] = [
                        "x"         => $staffName,
                        "y"         => [
                            $report,
                            $release
                        ],
//                        "fillColor" => '#008FFB'
                    ];
                }
            }
        }

        foreach ($data as $date => $arr) {
            foreach ($arr as $i => $item) {
                $this->staffCharts[$date][] = $item;
            }
        }
    }

    function fltChart(&$data, $fnReady, $time){
        $int = 60 * 60;

        $deptArrTime = strtotime($time);

        $initTime = strtotime(date("Y-m-d H:00:00", $deptArrTime));
        $endTime  = strtotime(date("Y-m-d H:59:59", $deptArrTime));

        for ($i = $initTime; $i <= $endTime; $i += $int){
            if ($deptArrTime >= $i && $deptArrTime < $i + $int){
                if (!isset($data[$i])) {
                    $data[$i] = [
                        "act"   => 0,
                        "flts"  => [],
                        "time"  => date("Y-m-d H:i", $i),
                    ];
                }

                // Activate
                $data[$i]["act"]++;
                $data[$i]["flts"][] = $fnReady;
            }
        }
    }

    protected $charts = [];

    /**
     * @return array
     */
    public function getCharts()
    {
        return $this->charts;
    }


    function setCharts(){

        $data = $fltData = [];

        $int = 15 * 60;

        foreach ($this->flightHandlers as $flightHandler) {
            // Timezone
            if ($this->timing == LOCAL){
                $deptInitPTD = utcToLocal($this->airportTimezones, $flightHandler->getStation()->id,  getFlightDepartureInitialDatePTD($flightHandler->getFlight(), true));
            }
            else {
                $deptInitPTD = getFlightDepartureInitialDatePTD($flightHandler->getFlight(), true);
            }

            $fnReady = getFlightNumberFull($flightHandler->getFlightNumber())."/".date("d", strtotime($deptInitPTD));
            $this->fltChart($fltData, $fnReady, $flightHandler->getServicedTime("Y-m-d H:i"));


            foreach ($flightHandler->getServiceList() as $j => $service){
                $reportTime = strtotime($service->getReportTimeTimezone());
                $releaseTime = strtotime($service->getReleaseTimeTimezone());

                $reportTimeInit = strtotime(date("Y-m-d H:0", strtotime($service->getReportTimeTimezone())));

                for($i = $reportTimeInit; $i <= $releaseTime; $i += $int){
                    if ($i + $int < $reportTime){
                        continue;
                    }
                    if ($reportTime >= $i && $reportTime < $i + $int){
                        $iDate = date("Y-m-d", $i);

                        if (!isset($data[$iDate][$i])){
                            $data[$iDate][$i] = [
                                "act"   => 0,
                                "req"   => 0,
                                "time"  => date("Y-m-d H:i", $i),
                                "sla"   => [],
                            ];
                        }

                        if (!isset($data[$iDate][$i]["act"])){
                            $data[$iDate][$i]["act"] = 0;
                        }
                        if (!isset($data[$iDate][$i]["req"])){
                            $data[$iDate][$i]["req"] = 0;
                        }

                        $data[$iDate][$i]["sla"][] = $service->getServiceName();

                        $data[$iDate][$i]["act"] += $service->getStaffActual();
                        $data[$iDate][$i]["req"] += $service->getStaffReq();

                    }
                }
            }

            if ($flightHandler->getServiceType() == ARRIVAL_SERVICE){
                $deptArrDateTime = getFlightArrivalInitialDate($flightHandler->getFlight());
            }
            else {
                $deptArrDateTime = getFlightDepartureInitialDate($flightHandler->getFlight());
            }

            // Set Timezone
            if ($this->timing == LOCAL){
                $deptArr = strtotime(utcToLocal($this->airportTimezones, $flightHandler->getStation()->id, $deptArrDateTime));
            }
            else {
                $deptArr = strtotime($deptArrDateTime);
            }
            //

        }

        ksort($fltData);
        $flightsChart = [];
        foreach ($fltData as $each) {
            $date = date("Y-m-d", strtotime($each["time"]));
            if (!isset($flightsChart[$date])){
                $flightsChart[$date] = [];
            }

            $flightsChart[$date][] = [
                "x" => date("H:i", strtotime($each["time"])),
                "y" => $each["act"],
            ];
        }

        $tableData = $charts = [];

        ksort($data);

        foreach ($data as $date => $arr ) {
            $charts[$date] = [
                "title"      => "SLA - Staff Requirements",
                "title2"     => "SLA - Staff Available",
//                "title3"     => "Flights",
            ];

            $tableData[$date] = [
                "time"  => [],
//                "flts"  => [],
            ];

            $resultArr = $arr;
            ksort($resultArr);

            foreach ($resultArr as $i => $each) {
                if (isset($each["req"])){
                    $charts[$date]["sla"][] = [
                        "x" => $each["time"],
                        "y" => $each["req"],
                    ];
                }

                if (isset($each["act"])){
                    $charts[$date]["sla2"][] = [
                        "x" => $each["time"],
                        "y" => $each["act"],
                    ];
                }

            }

        }

        $this->charts = [
            "charts"    => $charts,
            "table"     => $tableData,
            "flt"       => $flightsChart,
        ];
    }

    function loopMins(&$data, $timeMin){
        $mins = [
            0, 15, 30, 45
        ];

        $lastMinIndex = count($mins) - 1;

        for ($j = 0; $j < $lastMinIndex; $j++){
            if ($j == $lastMinIndex){
                if (!isset($data[$mins[$j]])){
                    $data[$mins[$j]] = 0;
                }
                $data[$mins[$j]]++;
            }
            else {
                if ($timeMin >= $mins[$j] && $timeMin < $mins[$j + 1]) {
                    if (!isset($data[$mins[$j]])){
                        $data[$mins[$j]] = 0;
                    }
                    $data[$mins[$j]]++;
                }
            }
        }
    }


    public function findFlightHandler($flightId){
        foreach($this->flightHandlers as $each){
            if ($each->getFlight()->id == $flightId){
                return $each;
            }
        }

        return null;
    }

    function orderByServiceStartTime(){

        $includedFlights = [];
        $finalFlights = [];

        $assignedCounter = count($this->assignedFlightIds);
        $manuallyCounter = count($this->manuallyRosteredFlightIds);

        foreach ($this->flights as $flight) {
            // Ignore Already Processed Flight
            if (in_array($flight->id, $includedFlights)){
                continue;
            }

            // Add Flight to Processed Flights Array
            $includedFlights[] = $flight->id;

            if ($assignedCounter && in_array($flight->id, $this->assignedFlightIds)){
                $flightHandler = $this->findFlightHandler($flight->id);
            }
            elseif ($manuallyCounter && in_array($flight->id, $this->manuallyRosteredFlightIds)){
                $flightHandler = $this->findFlightHandler($flight->id);
            }
            else {
                // Initiate Flight Handler
                $flightHandler = new FlightHandler($this, $flight);

                $this->flightHandlers[] = $flightHandler;
            }

            $finalFlights[$flight->id] = [
                "flight"                => $flight,
                "handler"               => $flightHandler,
                "first_service_time"    => null,
            ];

            foreach ($flightHandler->getServiceList() as $service) {
                if (!$finalFlights[$flight->id]["first_service_time"]){
                    $finalFlights[$flight->id]["first_service_time"] = strtotime($service->getReportTime());
                    continue;
                }

                if ($finalFlights[$flight->id]["first_service_time"] > strtotime($service->getReportTime())){
                    $finalFlights[$flight->id]["first_service_time"] = strtotime($service->getReportTime());
                }
            }
        }

        $this->helpers->prioritiesFirstServiceTime($finalFlights);

        $this->flights = $this->flightHandlers = [];
        foreach ($finalFlights as $each) {
            $this->flights[] = $each["flight"];
            //$debug[] = $each["first_service_time"];

            $this->flightHandlers[] = $each["handler"];
        }
//        debug($debug);
    }

    /**
     * @var FlightHandler $flightHandler
     * @return bool
     */
    function skipCurrentDateFlights($flightHandler){
        if ($flightHandler->getServiceType() == ARRIVAL_SERVICE){
//            debug($flightHandler->getFlightArrivalDate()." = ".date("Y-m-d"));
            return strtotime($flightHandler->getFlightArrivalDate()) <= strtotime(date("Y-m-d"));
        }
        else {
            return strtotime($flightHandler->getFlightDepartureDate()) <= strtotime(date("Y-m-d"));
        }
    }

    public function fillRegularFlight($flight, $method = 2){
        // Ignore Already Processed Flight
        if (in_array($flight->id, $this->processedFlights)){
            return;
        }

        // Add Flight to Processed Flights Array
        $this->processedFlights[] = $flight->id;

        $flightHandler = $this->findFlightHandler($flight->id);

        // Skip current date flights
        if ($this->skipCurrentDateFlights($flightHandler)){
            return;
        }

        // Staff Validation -1-
        // a) Get Staff For This Flight (Set SKIP = TRUE If Staff Can't Serve This Flight)
        // b) Prioritize if Flight Date Same As Staff's Current Shift Date
        $this->staffFilter->setFlight($flightHandler);

        // Also sets STAFF FILTER FILTERED STAFF LIST
        $this->filteredStaffList = $this->staffFilter->skipAndPrioritizeStaff();

        // $this->echoFlightInfo($i + 1, $flight);

        // Loop Through Flight's Services

        if ($this->settings->getAirportLicenseCheck() && isset($this->airportLicenses[$flightHandler->getStation()->id])){
            $airportLicenses = $this->airportLicenses[$flightHandler->getStation()->id];
            // Also sets STAFF FILTER FILTERED STAFF LIST
            $this->filteredStaffList = $this->staffFilter->checkAirportLicenseRequirements($airportLicenses, $flightHandler->getServicedDate());
        }

        // Check 5-day consequence roster limitation

        foreach ($flightHandler->getServiceList() as $j => $service) {

            if (!$service->getStaffReq()){
                continue;
            }

            debug($service->getServiceName()." | REQ:". $service->getStaffReq()." | ACT:". $service->getStaffActual());

            // Staff Validation -2-
            // a)Get Staff That Can Perform Service Function
            // b)Check Weekly And Monthly Hours Limit
            // c)Check If License Requirements Meets
            $this->eligibleStaffList = $this->staffFilter->checkStaffServicesLimitsLicenses($service);

            $this->eligibleStaffList = $this->staffFilter->checkConsequenceDaysLimitation($this->eligibleStaffList, $service);

            // Check if Leave Requests timing doesn't match
            $this->eligibleStaffList = $this->staffFilter->checkLeaveRequests($this->staffLeaves, $this->eligibleStaffList, $service);


            // Staff Validation -3-
            // Priorities Based On Percentage(%) of Max Hours Limit Accomplished
            $this->staffFilter->prioritiesShiftMinRestTime($this->eligibleStaffList);

            // Staff Validation -3b-
            // Priorities Based on Total Hours
            // $this->eligibleStaffList = $this->staffFilter->prioritiesTotalHours();

            // $this->printOutTotalHours($this->eligibleStaffList);

            // $this->echoFlightJobFunctionDetails($service);

            if ($method == 2){
                // Method Double Loop
                $this->method2($service, $flightHandler);
            }
            else {
                // Method Single Loop
                $this->method1($service, $flightHandler);
            }

            // Set Staff Count
            $flightHandler->setAllServicesStaffCount();

            // $service->printOutJobsDetails();
        }
    }

    /**
     * @var Service $service
     * @var FlightHandler $flightHandler
     */
    function method2(&$service, &$flightHandler){
        // Loop Through Each Service's Functions' Staff Requirements (Man Power)
        $staffRequirement = $service->getStaffReq();
        $staffAssigned    = $service->getStaffActual();

        // debug($this->eligibleStaffList);
        // Loop Through Prioritized Staff List

        $processedStaffIDs = [];

        // Priorities same flight staff
        foreach ($this->eligibleStaffList as $staff) {

            if (in_array($staff->userID, $processedStaffIDs)){
                continue;
            }

            if ($staffRequirement == $staffAssigned) {
                break;
            }

            // Add To existing Shift if possible
            if ($staff->getCurrentShift() && $staff->getCurrentShift()->getValidation()->validateService($service, $flightHandler->getFlightID())) {

                // Assign Service To Current Shift
                list($job, $k) = $staff->getCurrentShift()->assignService($service, $flightHandler);

                // Add Staff To Service StaffList
                $service->addJob($job, $k);

                $staffAssigned++;

                $processedStaffIDs[] = $staff->userID;
            }
        }

        // Continue with all eligible
        foreach ($this->eligibleStaffList as $staff) {

            if (in_array($staff->userID, $processedStaffIDs)){
                continue;
            }

            if ($staffRequirement == $staffAssigned) {
                break;
            }

            // Add To existing Shift if possible
            if ($staff->getCurrentShift() && $staff->getCurrentShift()->getValidation()->validateService($service)) {

                // Assign Service To Current Shift
                list($job, $k) = $staff->getCurrentShift()->assignService($service, $flightHandler);

                // Add Staff To Service StaffList
                $service->addJob($job, $k);

                $staffAssigned++;

                $processedStaffIDs[] = $staff->userID;
            }
        }

        $this->staffFilter->prioritiesByNightShiftsQty($this->eligibleStaffList);
//        shuffle($this->eligibleStaffList);

        foreach ($this->eligibleStaffList as $staff) {

            if (in_array($staff->userID, $processedStaffIDs)){
                continue;
            }

            if ($staffRequirement == $staffAssigned){
                break;
            }

            // Create New Shift
            if (!$staff->getCurrentShift() || $staff->getCurrentShift()->getValidation()->eligibleAsNewShift($service)){

                // Create New Shift And Assign Service
                list($job, $k) = $staff->createNewShift($service, $flightHandler);

                $service->addJob($job, $k);

                $staffAssigned++;
            }
        }
    }

    /**
     * @var Service $service
     * @var FlightHandler $flightHandler
     */
    function method1(&$service, &$flightHandler){
        // Loop Through Each Service's Functions' Staff Requirements (Man Power)
        $staffRequirement = $service->getStaffReq();
        $staffAssigned    = $service->getStaffActual();

        // debug($this->eligibleStaffList);
        // Loop Through Prioritized Staff List

        foreach ($this->eligibleStaffList as $staff) {

            if ($staffRequirement == $staffAssigned) {
                break;
            }

            // Add To existing Shift if possible
            if ($staff->getCurrentShift() && $staff->getCurrentShift()->getValidation()->validateService($service)) {

                // Assign Service To Current Shift
                list($job, $k) = $staff->getCurrentShift()->assignService($service, $flightHandler);

                // Add Staff To Service StaffList
                $service->addJob($job, $k);

                $staffAssigned++;

                continue;
            }

            // Create New Shift
            if (!$staff->getCurrentShift() || $staff->getCurrentShift()->getValidation()->eligibleAsNewShift($service)){

                // Create New Shift And Assign Service
                list($job, $k) = $staff->createNewShift($service, $flightHandler);

                $service->addJob($job, $k);

                $staffAssigned++;
            }
        }
    }

    /* @var RosterSettingsAssignedFlight $assignedFlight */
    public function fillAssignedFlight($assignedFlight){

        // Get Flight Handler
        $flightHandler = $assignedFlight->getFlightHandler();

        // $flight = $flightHandler->getFlight();

        // Staff Validation -1-
        // a) Get Staff For This Flight (Set SKIP = TRUE If Staff Can't Serve This Flight)
        // b) Prioritize if Flight Date Same As Staff's Current Shift Date
        $this->staffFilter->setFlight($flightHandler);

        // $this->filteredStaffList = $assignedFlight->getAssignedStaff();

        /* @var RosterSettingsAssignedService[] $assignedServices */
        $assignedServices = $assignedFlight->getAssignedServices();

        // $this->echoFlightInfo($i + 1, $flight);

        // Loop Through Flight's Services

        foreach ($assignedServices as $j => $assignedService) {

            $service = $assignedService->getStaffRosterService();

            // IF ASSIGNED SKIP
//            if (!$service->getStaffReq()){
//                continue;
//            }

            $this->filteredStaffList = [];
            foreach ($assignedService->getAssignedStaff() as $assignedStaff) {
                $this->filteredStaffList[] = $assignedStaff->getStaff();
            }

            // Set Staff Filters FILTERED STAFF LIST
            $this->staffFilter->setFilteredStaffList($this->filteredStaffList);

            // Staff Validation -2-
            // a)Get Staff That Can Perform Service Function
            // b)Check Weekly And Monthly Hours Limit
            // c)Check If License Requirements
//            SKIP CHECKING
//            $this->eligibleStaffList = $this->staffFilter->checkStaffServicesLimitsLicenses($service);
            $this->eligibleStaffList = $this->filteredStaffList;

            // $this->printOutTotalAccomplishmentHours($this->eligibleStaffList);

            // Staff Validation -3-
            // Priorities Based On Percentage(%) of Max Hours Limit Accomplished
            // SKIP CHECKING FOR ASSIGNED
//            $this->staffFilter->prioritiesShiftMinRestTime($this->eligibleStaffList);

            /*
            switch($this->settings->staffAssignment){
                case MAX_HOURS:
                    $this->staffFilter->prioritiesTotalHours($this->eligibleStaffList, true);
                    break;
                case MONTHLY_ACCOMPLISHMENT:
                    $this->staffFilter->prioritiesMonthlyLimitAccomplishmentHours($this->eligibleStaffList);
                    break;
                case TOTAL_HOURS:
                    $this->staffFilter->prioritiesTotalHours($this->eligibleStaffList, true);
                    break;
            }
            */

            // $this->printOutTotalAccomplishmentHours($this->eligibleStaffList);

            // Staff Validation -3b-
            // Priorities Based on Total Hours
            // $this->eligibleStaffList = $this->staffFilter->prioritiesTotalHours();
            // $this->printOutTotalHours($this->eligibleStaffList);
            // $this->echoFlightJobFunctionDetails($service);

            // Loop Through Each Service's Functions' Staff Requirements (Man Power)
            $staffRequirement = $service->getStaffReq();
            $staffAssigned    = 0;

            // debug($this->eligibleStaffList);
            // Loop Through Prioritized Staff List
            foreach ($this->eligibleStaffList as $staff) {

                if (!$staff->getCurrentShift()){

                    // Create New Shift And Assign Service
                    list($job, $k) = $staff->createNewShift($service, $flightHandler, ASSIGNED_TYPE);

                    $service->addJob($job, $k);

                    $staffAssigned++;

                    continue;
                }

                // Add To existing Shift if possible
                if ($staff->getCurrentShift()->getValidation()->validateAssignedService($service, $flightHandler->getFlightID())) {
                    // Assign Service To Current Shift
                    list($job, $k) = $staff->getCurrentShift()->assignService($service, $flightHandler, ASSIGNED_TYPE);

                    // Add Staff To Service StaffList
                    $service->addJob($job, $k);

                    $staffAssigned++;

                    continue;
                }

                if ($staff->getCurrentShift()->getValidation()->validateAssignedService($service)) {
                    // Assign Service To Current Shift
                    list($job, $k) = $staff->getCurrentShift()->assignService($service, $flightHandler, ASSIGNED_TYPE);

                    // Add Staff To Service StaffList
                    $service->addJob($job, $k);

                    $staffAssigned++;

                    continue;
                }

                // Create New Shift
                if ($staff->getCurrentShift()->getValidation()->eligibleAsNewShift($service)){

                    // Create New Shift And Assign Service
                    list($job, $k) = $staff->createNewShift($service, $flightHandler, ASSIGNED_TYPE);

                    $service->addJob($job, $k);

                    $staffAssigned++;

                    continue;
                }

                // Create New Shift And Assign Service
                list($job, $k) = $staff->createNewShift($service, $flightHandler, ASSIGNED_TYPE);

                $service->addJob($job, $k);

                $staffAssigned++;
                /// END
            }

            // Set Staff Count
            $flightHandler->setAllServicesStaffCount();

            // $service->printOutJobsDetails();
        }
    }

    /* @var ManualRosterFlight $manualFlight */
    public function fillManualFlight($manualFlight){

        // Get Flight Handler
        $flightHandler = $manualFlight->getFlightHandler();

        // $flight = $flightHandler->getFlight();

        // Staff Validation -1-
        // a) Get Staff For This Flight (Set SKIP = TRUE If Staff Can't Serve This Flight)
        // b) Prioritize if Flight Date Same As Staff's Current Shift Date
        $this->staffFilter->setFlight($flightHandler);

        // $this->filteredStaffList = $assignedFlight->getAssignedStaff();

        /* @var ManualRosterService[] $assignedServices */
        $assignedServices = $manualFlight->getRosteredServices();

        // $this->echoFlightInfo($i + 1, $flight);

        // Loop Through Flight's Services

        foreach ($assignedServices as $j => $assignedService) {

            $service = $assignedService->getStaffRosterService();

            if (!$service->getStaffReq()){
                continue;
            }

            $this->filteredStaffList = [];
            foreach ($assignedService->getManualStaff() as $manualStaff) {
                $this->filteredStaffList[] = $manualStaff->getStaff();
            }

            // Set Staff Filters FILTERED STAFF LIST
            $this->staffFilter->setFilteredStaffList($this->filteredStaffList);

            // Staff Validation -2-
            // a)Get Staff That Can Perform Service Function
            // b)Check Weekly And Monthly Hours Limit
            // c)Check If License Requirements
//            $this->eligibleStaffList = $this->staffFilter->checkStaffServicesLimitsLicenses($service);
//            SKIP CHECKING
            $this->eligibleStaffList = $this->filteredStaffList;

            // Loop Through Each Service's Functions' Staff Requirements (Man Power)
            $staffRequirement = $service->getStaffReq();
            $staffAssigned    = 0;

            // debug($this->eligibleStaffList);
            // Loop Through Prioritized Staff List
            foreach ($this->eligibleStaffList as $staff) {

                if (!$staff->getCurrentShift()){

                    // Create New Shift And Assign Service
                    list($job, $k) = $staff->createNewShift($service, $flightHandler, MANUALLY_ROSTERED_TYPE);

                    $service->addJob($job, $k);

                    $staffAssigned++;

                    continue;
                }

                // Add To existing Shift if possible
                if ($staff->getCurrentShift()->getValidation()->validateAssignedService($service, $flightHandler->getFlightID())) {
                    // Assign Service To Current Shift
                    list($job, $k) = $staff->getCurrentShift()->assignService($service, $flightHandler, MANUALLY_ROSTERED_TYPE);

                    // Add Staff To Service StaffList
                    $service->addJob($job, $k);

                    $staffAssigned++;

                    continue;
                }

                if ($staff->getCurrentShift()->getValidation()->validateAssignedService($service)) {
                    // Assign Service To Current Shift
                    list($job, $k) = $staff->getCurrentShift()->assignService($service, $flightHandler, MANUALLY_ROSTERED_TYPE);

                    // Add Staff To Service StaffList
                    $service->addJob($job, $k);

                    $staffAssigned++;

                    continue;
                }

                // Create New Shift
                if ($staff->getCurrentShift()->getValidation()->eligibleAsNewShift($service)){

                    // Create New Shift And Assign Service
                    list($job, $k) = $staff->createNewShift($service, $flightHandler, MANUALLY_ROSTERED_TYPE);

                    $service->addJob($job, $k);

                    $staffAssigned++;

                    continue;
                }

                // Create New Shift And Assign Service
                list($job, $k) = $staff->createNewShift($service, $flightHandler, MANUALLY_ROSTERED_TYPE);

                $service->addJob($job, $k);

                $staffAssigned++;
            }

            // Set Staff Count
            $flightHandler->setAllServicesStaffCount();

            // $service->printOutJobsDetails();
        }
    }

    public function getFromDate(){
        return $this->fromDate;
    }

    public function getToDate(){
        return $this->toDate;
    }

    /**
     * @return mixed
     */
    public function getAirport()
    {
        return $this->airport;
    }

    /**
     * @return Flight[]
     */
    public function getFlights()
    {
        return $this->flights;
    }

    /**
     * @return mixed
     */
    public function getAirlines()
    {
        return $this->airlines;
    }

    /**
     * @return FlightHandler[]
     */
    public function getFlightHandlers()
    {
        return $this->flightHandlers;
    }

    /**
     * @return User[]
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * @return Staff[]
     */
    public function getStaffList()
    {
        return $this->staffList;
    }

    /**
     * @return StaffFilter
     */
    public function getStaffFilter()
    {
        return $this->staffFilter;
    }

    /**
     * @return mixed
     */
    public function getHandlingStations()
    {
        return $this->handlingStations;
    }

    /**
     * @return mixed
     */
    public function getServicesList()
    {
        return $this->servicesList;
    }

    /**
     * @return mixed
     */
    public function getServicesByAirlineAndAirports()
    {
        return $this->servicesByAirlineAndAirports;
    }

    public function getAirportLicenseRequirements(){
        return $this->airportLicenses;
    }

    public function getAirlineLicenseRequirements(){
        return $this->airlineLicenses;
    }

    public function addFlightHandler($value){
        $this->flightHandlers[] = $value;
    }

    public function setFromDate($value){
        $this->fromDate = $value;
    }

    public function setToDate($value){
        $this->toDate = $value;
    }

    public function setAirport($value){
        $this->airport = $value;
    }

    public function setFlights($value){
        $this->flights = $value;
    }

    public function setAirlines($value){
        $this->airlines = $value;
    }

    public function setFlightHandlers($value){
        $this->flightHandlers = $value;
    }

    public function setUsers($value){
        $this->users = $value;
    }

    public function setStaffList($value){
        $this->staffList = $value;
    }

    public function addStaffToStaffList($value){
        $this->staffList[] = $value;
    }

    public function setStaffFilter($value){
        $this->staffFilter = $value;
    }

    public function setHandlingStations($value){
        $this->handlingStations = $value;
    }

    public function setServicesList($value){
        $this->servicesList = $value;
    }

    public function setServicesByAirlineAndAirports($value){
        $this->servicesByAirlineAndAirports = $value;
    }

    public function setServicesLicenseRequirements($value){
        $this->airlineLicenses = $value;
    }

    /* @var Staff[] $list */
    public function printOutTotalAccomplishmentHours($list)
    {
        foreach ($list as $staff) {
            echo $staff->getValidation()->getMonthlyLimitAccomplishment() . " , ";
        }
        echo "<br/>";
    }

    /**
     * @var Staff[] $list
     * @var Service $service
     */
    public function debugStaffShiftEndTimes($airline, $service, $list)
    {
        $data = [
            "{$airline->iata} - {$service->getServiceName()} | STARTS: ". $service->getReportTime()
        ];
        foreach ($list as $staff) {
            if (!$staff->getCurrentShift()){
                $data[] = $staff->getName(). " | NO SHIFT";
                continue;
            }

            $data[] = $staff->getName(). " | SHIFT ENDS: ".$staff->getCurrentShift()->getEndTime()." | VALID FOR SERVICE: ".($staff->getCurrentShift()->getValidation()->validateService($service) ? "YES" : "NO");
        }

        debug($data);
    }

    /* @var Staff[] $list */
    public function printOutTotalHours($list){
        foreach ($list as $staff) {
            echo $staff->getTotalShiftHours()." , ";
        }
        echo "<br/>";
    }

    public function echoStaffJobDetails(){
        echo "<strong>---------------------------STAFF DETAILS-------------------------</strong><br/><br/>".
            "<table>" .
            "<tr>" .
                "<th>ID</th>".
                "<th>Name</th>".
                "<th>Accomplishment</th>".
                "<th>Shifts</th>".
                "<th>Total Hours</th>".
                "<th>Weekly Limit</th>".
                "<th>Monthly Limit</th>".
            "</tr>";

        foreach ($this->staffList as $staff) {
//            if ($staff->getShiftCount() > 0){
                $staff->printOutDetails();
//            }
        }

        echo "</table>";

    }

    public function echoFlightInfo($i, $flight){
        echo $i.")"." Flight: ". date("M d", strtotime($flight->std)). " | ". $flight->flightNumber->flight_number."<br/>".
            "FILTER STAFF COUNT: <strong>". count($this->filteredStaffList)."</strong><br/>";
    }

    /* @var $service Service */
    public function echoFlightJobFunctionDetails($service){
        echo "ELIGIBLE STAFF COUNT: <strong>". count($this->eligibleStaffList)."</strong><br/>";

//        echo $service->getServiceType()
//            ." | Function:  ".  $service->getServiceName()
//            ." | Report:    ".  $service->getReportTime()
//            ." | Release:   ".  $service->getReleaseTime()
//            ."<br/>Staff Req: <strong>".  $service->getStaffReq()."</strong>"
//            ." | Staff Min: ".  $service->getStaffMin()
//            ."<br/>";

    }
}