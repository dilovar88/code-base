<?php namespace App\Classes\Staff\Roster;

use App\Classes\Staff\Roster\ServiceTypes\ArrivalService;
use App\Classes\Staff\Roster\ServiceTypes\DepartureService;
use App\Classes\Staff\Roster\ServiceTypes\Service;
use App\Classes\Staff\Roster\ServiceTypes\TurnaroundService;
use App\Models\Airline;
use App\Models\Airport;
use App\Models\Flight;
use App\Models\FlightNumber;
use Illuminate\Database\Eloquent\Model;

/**
 * Created by PhpStorm.
 */
class FlightHandler extends Model
{
    protected $table = "staff_roster__flight_handlers";

    public $timestamps = true;

    public function servicesDB(){
        return $this->hasMany(Service::class, "flight_handler_id");
    }

    // EXCLUDED FROM DB
    /* @var Roster $roster */
    protected $roster;

    protected $sector;

    protected $servicesList;

    protected $airlineLicenseRequirements;

    // Handling Airports
    protected $handlingStations;

    protected $airlineServices;

    // Current Airline Instance
    /* @var Airline $airline  */
    protected $airline;

    // Current Flight Number Instance
    /* @var FlightNumber $flightNumber  */
    protected $flightNumber;

    // IN DB

    // FLIGHT_ID

    protected $flightDepartureDate;

    protected $flightDepartureTime;

    protected $flightDepartureTimeType;

    protected $flightDepartureTimeActual;

    protected $flightDepartureTimeTypeActual;

    protected $flightArrivalDate;

    protected $flightArrivalTime;

    protected $flightArrivalTimeType;

    protected $flightArrivalTimeActual;

    protected $flightArrivalTimeTypeActual;

    // Departure/Arrival Service Type
    protected $serviceType;

    // All Services Total Staff Requirement
    protected $staffReq;

    // All Services Total Staff Minimum Count
    protected $staffMin;

    // All Services Total Staff Actual Rostered Count
    protected $staffAct;

    // List of All Services
    /* @var \App\Classes\Staff\Roster\ServiceTypes\Service[] $serviceList */
    protected $serviceList = [];

    /* @var Flight $flight  */
    protected $flight;

    // Current Airport(Station) Instance
    /* @var Airport $station  */
    protected $station;

//    public $assignedManually;

    public $staffType;

    /**
     * FlightHandler constructor.
     * @param Roster $roster
     * @param Flight $flight
     * @param int $staffType
            * values 1-manually rostered
     */
    public function __construct($roster = null, $flight = null, $staffType = null){

        parent::__construct();

        if ($roster && $flight) {

            $this->roster = $roster;

//            $this->assignedManually = $assignedManually;

            $this->staffType = $staffType;

            // Set Current Flight
            $this->setFlight($flight);

            // Set Current Airline And Flight Number
            $this->setFlightNumberAndAirline();

            // $this->servicesList = $roster->getServicesList();

            // Set All Handling Stations
            $this->handlingStations = $roster->getHandlingStations();

            $this->airlineLicenseRequirements = $roster->getAirlineLicenseRequirements();

            // Set Current Station And Service Type
            $this->setServiceTypeAndStation();

            $airlineServices = $roster->getServicesByAirlineAndAirports();
            if (isset($airlineServices[$this->airline->id][$this->station->id])) {
                $this->airlineServices = $airlineServices[$this->airline->id][$this->station->id];
            }

            $this->setFlightDepartureTime();

            $this->setFlightArrivalTime();

            // Set All Services
            $this->setAllServices();
        }

    }

    public function saveResult()
    {
        // Save Current Roster
        $this->roster_id = $this->roster->id;
        $this->flight_id = $this->flight->id;
        $this->station_id = $this->station->id;
        $this->departure_time = $this->flightDepartureTime;
        $this->departure_type = $this->flightDepartureTimeType;
        $this->arrival_time = $this->flightArrivalTime;
        $this->arrival_type = $this->flightArrivalTimeType;
        $this->service_type = $this->serviceType;
        $this->staff_req = $this->staffReq;
        $this->staff_min = $this->staffMin;
        $this->staff_act = $this->staffAct;
//        $this->assigned = $this->assignedManually;
        $this->staff_type = $this->staffType;
        $this->save();
    }

    public function loadFromDB($roster, $flightsByIDs, $airportsByIDs)
    {
        $this->roster = $roster;

        // Save Current Roster
        $this->flight = isset($flightsByIDs[$this->flight_id]) && $flightsByIDs[$this->flight_id] ? $flightsByIDs[$this->flight_id]
                                            : Flight::with([
                                                'flightNumber',
                                                'flightNumber.departureAirport',
                                                'flightNumber.arrivalAirport'])
                                            ->find($this->flight_id);

        $this->flightNumber = $this->flight->flightNumber;
        $this->setFlightNumberAndAirline();

        $this->station = isset($airportsByIDs[$this->station_id]) && $airportsByIDs[$this->station_id] ? $airportsByIDs[$this->station_id] : Airport::find($this->station_id);

        $this->flightDepartureTime = $this->departure_time;
        $this->flightDepartureTimeType = $this->departure_type;
        $this->flightArrivalTime = $this->arrival_time;
        $this->flightArrivalTimeType = $this->arrival_type;
        $this->serviceType = $this->service_type;
        $this->staffReq = $this->staff_req;
        $this->staffMin = $this->staff_min;
        $this->staffAct = $this->staff_act;
//        $this->assignedManually = $this->assigned;
        $this->staffType = $this->staff_type;
    }

    /**
     * @return Roster $roster
     */
    public function getRoster()
    {
        return $this->roster;
    }

    public function getFlight(){
        return $this->flight;
    }

    public function getFlightID(){
        return $this->flight->id;
    }

    public function getFlightNumber(){
        return $this->flightNumber;
    }

    public function getFlightNumberReady(){
        return getFlightNumberFull($this->flightNumber);
    }

    public function getSector(){
        return $this->sector;
    }

    public function getServiceType(){
        return $this->serviceType;
    }

    public function getFlightDepartureTime(){
        return $this->flightDepartureTime;
    }

    public function getFlightDepartureTimeTimezone(){
        if ($this->roster->timing == LOCAL){
            return utcToLocal($this->roster->airportTimezones, $this->getStation()->id, $this->flightDepartureTime);
        }

        return $this->flightDepartureTime;
    }


    public function getFlightDepartureTimeType(){
        return $this->flightDepartureTimeType ? strtoupper($this->flightDepartureTimeType) : "";
    }

    public function getFlightDepartureTimeActual(){
        return $this->flightDepartureTimeActual;
    }

    public function getFlightDepartureTimeActualTimezone(){
        if ($this->roster->timing == LOCAL){
            return utcToLocal($this->roster->airportTimezones, $this->getStation()->id, $this->flightDepartureTimeActual);
        }

        return $this->flightDepartureTimeActual;
    }


    public function getFlightDepartureTimeTypeActual(){
        return $this->flightDepartureTimeTypeActual ? strtoupper($this->flightDepartureTimeTypeActual) : "";
    }

    public function getFlightArrivalTime(){
        return $this->flightArrivalTime;
    }

    public function getFlightArrivalTimeTimezone(){
        if ($this->roster->timing == LOCAL){
            return utcToLocal($this->roster->airportTimezones, $this->getStation()->id, $this->flightArrivalTime);
        }

        return $this->flightArrivalTime;
    }



    /**
     * @return mixed
     */
    public function getFlightArrivalDate()
    {
        return $this->flightArrivalDate;
    }

    /**
     * @return mixed
     */
    public function getFlightDepartureDate()
    {
        return $this->flightDepartureDate;
    }

    public function getFlightArrivalTimeType(){
        return $this->flightArrivalTimeType ? strtoupper($this->flightArrivalTimeType) : "";
    }

    public function getFlightArrivalTimeActual(){
        return $this->flightArrivalTimeActual;
    }

    public function getFlightArrivalTimeActualTimezone(){
        if ($this->roster->timing == LOCAL){
            return utcToLocal($this->roster->airportTimezones, $this->getStation()->id, $this->flightArrivalTimeActual);
        }

        return $this->flightArrivalTimeActual;
    }

    public function getFlightArrivalTimeTypeActual(){
        return $this->flightArrivalTimeTypeActual ? strtoupper($this->flightArrivalTimeTypeActual) : "";
    }

    /**
     * @return Service[]
     */
    public function getServiceList(){
        return $this->serviceList;
    }

    public function getRosterServiceByServiceId($serviceId){
        foreach ($this->serviceList as $each) {
            if ($each->getServiceId() == $serviceId){
                return $each;
            }
        }

        return null;
    }

    public function getAirline(){
        return $this->airline;
    }

    public function getStation(){
        return $this->station;
    }

    public function getAirlineServices(){
        return $this->airlineServices;
    }

    public function getStaffReq(){
        return $this->staffReq;
    }

    public function getStaffMin(){
        return $this->staffMin;
    }

    public function getStaffAct(){
        return $this->staffAct;
    }

    public function setFlight($value){
        $this->flight = $value;
    }

    public function setServiceList($value){
        return $this->serviceList = $value;
    }

    public function setFlightDepartureTime(){
        // Init
        if ($this->flight->ptd && $this->flight->ptd != EMPTY_DATETIME){
            $this->flightDepartureTimeType = PTD;
            $this->flightDepartureTime = $this->flight->ptd;
        }
        else if ($this->flight->std && $this->flight->std != EMPTY_DATETIME){
            $this->flightDepartureTimeType = STD;
            $this->flightDepartureTime = $this->flight->std;
        }
        else if ($this->flight->atd && $this->flight->atd != EMPTY_DATETIME){
            $this->flightDepartureTimeType = ATD;
            $this->flightDepartureTime = $this->flight->atd;
        }
        else if ($this->flight->etd && $this->flight->etd != EMPTY_DATETIME){
            $this->flightDepartureTimeType = ETD;
            $this->flightDepartureTime = $this->flight->etd;
        }

        if ($this->flightDepartureTime){
            $this->flightDepartureDate = date("Y-m-d", strtotime($this->flightDepartureTime));
        }

        // Set Actual
        if ($this->flight->atd && $this->flight->atd != EMPTY_DATETIME){
            $this->flightDepartureTimeTypeActual = ATD;
            $this->flightDepartureTimeActual = $this->flight->atd;
        }
        else if ($this->flight->etd && $this->flight->etd != EMPTY_DATETIME){
            $this->flightDepartureTimeTypeActual = ETD;
            $this->flightDepartureTimeActual = $this->flight->etd;
        }
    }

    public function setFlightArrivalTime(){
        if ($this->flight->pta && $this->flight->pta != EMPTY_DATETIME){
            $this->flightArrivalTimeType = PTA;
            $this->flightArrivalTime = $this->flight->pta;
        }
        elseif ($this->flight->sta && $this->flight->sta != EMPTY_DATETIME){
            $this->flightArrivalTimeType = STA;
            $this->flightArrivalTime = $this->flight->sta;
        }
        elseif ($this->flight->ata && $this->flight->ata != EMPTY_DATETIME){
            $this->flightArrivalTimeType = ATA;
            $this->flightArrivalTime = $this->flight->ata;
        }
        elseif ($this->flight->eta && $this->flight->eta != EMPTY_DATETIME){
            $this->flightArrivalTimeType = ETA;
            $this->flightArrivalTime = $this->flight->eta;
        }

        if ($this->flightArrivalTime){
            $this->flightArrivalDate = date("Y-m-d", strtotime($this->flightArrivalTime));
        }

        // Set Actual
        if ($this->flight->ata && $this->flight->ata != EMPTY_DATETIME){
            $this->flightArrivalTimeTypeActual = ATA;
            $this->flightArrivalTimeActual = $this->flight->ata;
        }
        elseif ($this->flight->eta && $this->flight->eta != EMPTY_DATETIME){
            $this->flightArrivalTimeTypeActual = ETA;
            $this->flightArrivalTimeActual = $this->flight->eta;
        }
    }

    public function getServicedDate(){
        if ($this->serviceType == DEPARTURE_SERVICE){
            return $this->flightDepartureDate;
        }
        else {
            return $this->flightArrivalDate;
        }
    }

    public function getServicedTime($timeFormat = "H:i"){
        if ($this->serviceType == DEPARTURE_SERVICE){
            return date($timeFormat, strtotime($this->flightDepartureTime));
        }
        else {
            return date($timeFormat, strtotime($this->flightArrivalTime));
        }
    }

    public function getDepartureAirport(){
        return $this->flightNumber->departureAirport ? $this->flightNumber->departureAirport->iata : "";
    }

    public function getArrivalAirport(){
        return $this->flightNumber->arrivalAirport ? $this->flightNumber->arrivalAirport->iata : "";
    }

    public function getOriginDestinationAirport(){
        if ($this->serviceType == DEPARTURE_SERVICE){
            return $this->getArrivalAirport();
        }
        else {
            return $this->getDepartureAirport();
        }
    }

    public function setFlightNumberAndAirline(){

        if (!$this->flight){
            return;
        }

        $this->flightNumber = $this->flight->flightNumber;

        if ($this->flightNumber){

            $this->airline = $this->flightNumber->airline;

            $sector = $this->flightNumber->departureAirport ? $this->flightNumber->departureAirport->iata : "";
            $sector .= "-".($this->flightNumber->arrivalAirport ? $this->flightNumber->arrivalAirport->iata : "");

            $this->sector = $sector;
        }
    }

    public function setServiceTypeAndStation(){
        if (!$this->flightNumber || !$this->handlingStations){
            return;
        }

        // Roster Station
        $airportID = $this->roster->getAirport();

        $departureAirportID = $this->flight->departure_airport_id ? $this->flight->departure_airport_id : $this->flightNumber->departure_airport_id;
        $arrivalAirportID = $this->flight->arrival_airport_id ? $this->flight->arrival_airport_id : $this->flightNumber->arrival_airport_id;

        if ($airportID == $departureAirportID &&
            array_key_exists($departureAirportID, $this->handlingStations))
        {
            $this->station = $this->handlingStations[$airportID];
            $this->serviceType = DEPARTURE_SERVICE;
        }
        elseif ($airportID == $arrivalAirportID &&
            array_key_exists($arrivalAirportID, $this->handlingStations))
        {
            $this->station = $this->handlingStations[$airportID];
            $this->serviceType = ARRIVAL_SERVICE;
        }
    }


    public function setAllServices(){
        // Find All Functions For The Current Airline in Current Station

        $airlineServices = $this->getAirlineServices();

        if (!$airlineServices || count($airlineServices) == 0){
            return;
        }


        if ($this->serviceType == ARRIVAL_SERVICE){
            $depArrTime = $this->getFlightArrivalTime();
        }
        else {
            $depArrTime = $this->getFlightDepartureTime();
        }


        foreach ($airlineServices as $each) {

            if (!checkSLAEffectivePeriod($each, $depArrTime)){
                continue;
            }

            $airlineLicenseRequirements = null;

            if (isset($this->airlineLicenseRequirements[$this->airline->id][$each->service_id])){
                $airlineLicenseRequirements = $this->airlineLicenseRequirements[$this->airline->id][$each->service_id];
            }

            if ($each){
                $itemServiceType = $this->getAirlineServiceType($each, $this->serviceType);
                if (!$itemServiceType){
                    continue;
                }

                switch($itemServiceType){
                    case DEPARTURE_SERVICE:
                        if ($each->service_timings && $each->service->departure_service ||
                            (!$each->service_timings && $each->departure_service)) {
                            $this->serviceList[] = new DepartureService($this, $each, $airlineLicenseRequirements);
                        }
                        break;

                    case ARRIVAL_SERVICE:
                        if ($each->service_timings && $each->service->arrival_service ||
                            (!$each->service_timings && $each->arrival_service)){
                            $this->serviceList[] = new ArrivalService($this, $each, $airlineLicenseRequirements);
                        }
                        break;

                    case TURNAROUND_SERVICE:
                        if ($each->service_timings && $each->service->turnaround_service ||
                            (!$each->service_timings && $each->turnaround_service)){
                            $this->serviceList[] = new TurnaroundService($this, $each, $airlineLicenseRequirements);
                        }
                        break;
                }
            }
        }
    }

    function getAirlineServiceType($airlineService, $serviceType){
        if ($serviceType == ARRIVAL_SERVICE){
            $service_type = ARRIVAL_SERVICE;

            if ( ($airlineService->service_timings && !$airlineService->service->arrival_service)
                || (!$airlineService->service_timings && !$airlineService->arrival_service)
            ){
                return null;
            }
        }
        else {
            $service_type = DEPARTURE_SERVICE;
            if ( ($airlineService->service_timings && !$airlineService->service->departure_service)
                || (!$airlineService->service_timings && !$airlineService->departure_service)
            ){
                if (($airlineService->service_timings && !$airlineService->service->turnaround_service)
                    || (!$airlineService->service_timings && !$airlineService->turnaround_service)){
                    return null;
                }
                else {
                    if (staffFlightIsInactiveByID($this->flight->parent_id)){
                        return null;
                    }
                    else {
                        $service_type = TURNAROUND_SERVICE;
                    }
                }
            }
        }

        return $service_type;
    }

    /**
     * Set All Services Staff Count(Req, Min, Act)
     */
    public function setAllServicesStaffCount(){
        $req = $min = $act = 0;

        foreach ($this->getServiceList() as $j => $service) {

            $req += $service->getStaffReq();

            $min += $service->getStaffMin();

            $act += $service->getStaffActual();
        }

        $this->staffReq = $req;

        $this->staffMin = $min;

        $this->staffAct = $act;

//        debug("FLT ID: ".$this->getFlightID(). " / REQ: {$req}/ MIN: {$min}/ ACT: {$act}");
    }

    // ---- Coming Functions:
    // Get All Services With No Staff
    // Get All Functions With Min Staff Requirement Not Met
    // Get All Functions With Min Staff Requirement Met
}