<?php
namespace App\Classes\Staff\Roster\ManuallyRostered;

use App\Classes\Staff\Roster\ServiceTypes\Service;
use App\Classes\Staff\Roster\Staff\Staff;

class ManualRosterService
{
    public $timestamps = true;

    /* @var ManualRosterFlight $manualFlight*/
    protected $manualFlight;

    /* @var Service $staffRosterService */
    protected $staffRosterService;

    /* @var ManualRosterStaff[] $manualStaff */
    protected $manualStaff = [];

    /**
     * @var ManualRosterFlight $manualFlight
     * @var Service $staffRosterService
     * @var Staff[] $staffList
     */
    public function __construct($manualFlight = null, $staffRosterService = null, $staffList = null){

        $this->manualFlight = $manualFlight;
        $this->staffRosterService = $staffRosterService;

        if ($staffList && count($staffList)){
            foreach ($staffList as $staff) {
                $this->manualStaff[] = new ManualRosterStaff($this, $staff);
            }
        }
    }

    /**
     * @return ManualRosterFlight
     */
    public function getManualFlight()
    {
        return $this->manualFlight;
    }

    /**
     * @return ManualRosterStaff[]
     */
    public function getManualStaff()
    {
        return $this->manualStaff;
    }

    public function getStaffRosterService(){
        return $this->staffRosterService;
    }

    /**
     * @var Service[] $staffRosterServicesArray
     * @param $staffRosterServiceId
     * @return null
     */
    public function findStaffRosterService($staffRosterServicesArray, $staffRosterServiceId){
        if ($staffRosterServicesArray && count($staffRosterServicesArray) && $staffRosterServiceId){
            foreach ($staffRosterServicesArray as $each) {
                if ($each->id == $staffRosterServiceId){
                    return $each;
                }
            }
        }

        return null;
    }
}
