<?php
namespace App\Classes\Staff\Roster\ManuallyRostered;

use App\Classes\Staff\Roster\FlightHandler;
use App\Classes\Staff\Roster\ServiceTypes\Service;
use App\Classes\Staff\Roster\Staff\Staff;
use App\Models\Flight;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class ManualRosterFlight
{
    /* @var ManualRoster $manualRoster */
    protected $manualRoster;

    protected $rosteredServices = [];

    /* @var FlightHandler $flightHandler */
    protected $flightHandler;

    /**
     * @var ManualRoster $manualRoster
     * @var FlightHandler $flightHandler
     * @param null $servicesAndStaffList
     */
    public function __construct($manualRoster = null, $flightHandler = null, $servicesAndStaffList = null){

        $this->manualRoster = $manualRoster;

        $this->flightHandler = $flightHandler;

        if ($servicesAndStaffList){
            $this->addServicesAndStaff($servicesAndStaffList);
        }
    }

    /**
     * @return ManualRoster
     */
    public function getManualRoster()
    {
        return $this->manualRoster;
    }

    public function getFlightHandler()
    {
        return $this->flightHandler;
    }

    /**
     * @return array
     */
    public function getRosteredServices()
    {
        return $this->rosteredServices;
    }


    public function addServicesAndStaff($servicesAndStaffList){
        if ($servicesAndStaffList && count($servicesAndStaffList)){
            foreach ($servicesAndStaffList as $serviceId => $userIds) {

                $rosterService = $this->flightHandler->getRosterServiceByServiceId($serviceId);

                if ($rosterService) {

                    $staffList = [];
                    foreach ($userIds as $eachId) {

                        $staff = $this->manualRoster->getRoster()->getStaffObjectByUserId($eachId);

                        if ($staff) {
                            $staffList[] = $staff;
                        }
                    }

                    $this->rosteredServices[] = new ManualRosterService($this, $rosterService, $staffList);
                }
            }
        }
    }

    /**
     * @var FlightHandler[] $flightHandlers
     * @param $flightHandlerId
     * @return null
     */
    public function findFlightHandler($flightHandlers, $flightHandlerId){
        if ($flightHandlers && count($flightHandlers) && $flightHandlerId){
            foreach ($flightHandlers as $each) {
                if ($each->id == $flightHandlerId){
                    return $each;
                }
            }
        }

        return null;
    }
}
