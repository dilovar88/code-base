<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 03.02.17
 */

namespace App\Classes\Staff\Roster\ManuallyRostered;

use App\Classes\Staff\Roster\Staff\Staff;

class ManualRosterStaff
{
    /* @var ManualRosterService $manualService */
    protected $manualService;

    /* @var Staff $staff */
    protected $staff;

    /**
     * @var ManualRosterService $manualService
     * @var Staff $staff
    */
    public function __construct($manualService = null, $staff = null){

        $this->manualService = $manualService;
        $this->staff = $staff;
    }

    /**
     * @return ManualRosterService
     */
    public function getManualService()
    {
        return $this->manualService;
    }

    public function getStaff(){
        return $this->staff;
    }

    /**
     * @var Staff[] $staffArray
     * @param $staffRosterStaffId
     * @return null
     */
    public function findStaff($staffArray, $staffRosterStaffId){
        if ($staffArray && count($staffArray) && $staffRosterStaffId){
            foreach ($staffArray as $each) {
                if ($each->id == $staffRosterStaffId){
                    return $each;
                }
            }
        }

        return null;
    }
}
