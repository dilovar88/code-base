<?php
namespace App\Classes\Staff\Roster\ManuallyRostered;

use App\Classes\Staff\Roster\FlightHandler;
use App\Classes\Staff\Roster\Roster;
use App\Models\Flight;

class ManualRoster
{
    protected $shiftMaxHours;
    protected $shiftMinHours;
    protected $shiftMinRestTime;
    protected $staffAssignment;
    protected $airlineLicenseCheck;
    protected $airportLicenseCheck;

    /* @var Roster $roster */
    protected $roster;

    protected $manuallyRosteredFlights = [];

    public function __construct($roster = null){

        $this->roster = $roster;
    }

    // GET
    public function getRoster(){
        return $this->roster;
    }

    /**
     * @return ManualRosterFlight[]
     */
    public function getManuallyRosteredFlights()
    {
        return $this->manuallyRosteredFlights;
    }

    public function getShiftMaxHours(){
        return $this->shiftMaxHours;
    }

    public function getShiftMinHours(){
        return $this->shiftMinHours;
    }

    public function getShiftMinRestTime(){
        return $this->shiftMinRestTime;
    }

    public function getStaffAssignment(){
        return $this->staffAssignment;
    }

    public function getAirlineLicenseCheck(){
        return $this->airlineLicenseCheck;
    }

    public function getAirportLicenseCheck(){
        return $this->airportLicenseCheck;
    }


    /**
     * @param array $manuallyRosteredFlights
     */
    public function setManuallyRosteredFlights($manuallyRosteredFlights)
    {
        $this->manuallyRosteredFlights = $manuallyRosteredFlights;
    }

    public function setShiftMaxHours($value){
        $this->shiftMaxHours = $value;
    }

    public function setShiftMinHours($value){
        $this->shiftMinHours = $value;
    }

    public function setShiftMinRestTime($value){
        $this->shiftMinRestTime = $value;
    }

    public function setStaffAssignment($value){
        $this->staffAssignment = $value;
    }

    public function setAirlineLicenseCheck($value){
        $this->airlineLicenseCheck = $value ? true : false;
    }

    public function setAirportLicenseCheck($value){
        $this->airportLicenseCheck = $value ? true : false;
    }

    public function addFlight($flightId, $servicesAndStaffList = null){

        $flight = Flight::find($flightId);
        // Initiate Flight Handler
        $flightHandler = new FlightHandler($this->getRoster(), $flight, MANUALLY_ROSTERED_TYPE);

        $this->roster->addFlightHandler($flightHandler);

        $this->manuallyRosteredFlights[] = new ManualRosterFlight($this, $flightHandler, $servicesAndStaffList);
    }
}
