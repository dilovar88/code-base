<?php namespace App\Classes\Staff\Roster\ServiceTypes;

use App\Classes\Staff\Roster\FlightHandler;
use App\Models\AirlineService;

class ArrivalService extends Service implements IService
{
    /**
     * @var FlightHandler $flightHandler
     * @var AirlineService $serviceRecord
     * @var $serviceLicenseRequirements
     */
    public function __construct($flightHandler, $serviceRecord, $serviceLicenseRequirements){

        parent::__construct($flightHandler, $serviceRecord, $serviceLicenseRequirements);

        $this->setParameters($serviceRecord);
    }

    public function setParameters($serviceRecord){

        if (!$serviceRecord){
            return;
        }

        if ($serviceRecord->service_timings && $serviceRecord->service){
            $staffReq       = $serviceRecord->service->arr_staff_req;
            $staffMin       = $serviceRecord->service->arr_staff_min;
            $reportTimeMin  = $serviceRecord->service->arr_report_time;
            $releaseTimeMin = $serviceRecord->service->arr_release_time;
        }
        else {
            $staffReq       = $serviceRecord->arr_staff_req;
            $staffMin       = $serviceRecord->arr_staff_min;
            $reportTimeMin  = $serviceRecord->arr_report_time;
            $releaseTimeMin = $serviceRecord->arr_release_time;
        }

        $this->setStaffReq($staffReq ? $staffReq : $staffMin);

        $this->setStaffMin($staffMin ? $staffMin : $staffReq);

        $this->setReportTimeMin($reportTimeMin);

        $this->setReleaseTimeMin($releaseTimeMin);

        $this->setFlightArrivalTime();

        $this->setDuration();

        $this->setReportTime();

        $this->setReleaseTime();

    }

    public function setFlightArrivalTime(){
        if ($this->flight->pta && $this->flight->pta != EMPTY_DATETIME){
            $this->flightArrivalTime = $this->flight->pta;
        }
        elseif ($this->flight->sta && $this->flight->sta != EMPTY_DATETIME){
            $this->flightArrivalTime = $this->flight->sta;
        }
        elseif ($this->flight->ata && $this->flight->ata != EMPTY_DATETIME){
            $this->flightArrivalTime = $this->flight->ata;
        }
        elseif ($this->flight->eta && $this->flight->eta != EMPTY_DATETIME){
            $this->flightArrivalTime = $this->flight->eta;
        }
    }

    public function setDuration(){
        if (!$this->serviceType){
            return;
        }

        $this->serviceDuration = $this->reportTimeMin + $this->releaseTimeMin;
    }

    public function setReportTime(){
        if (!$this->flightArrivalTime){
            return;
        }

        // Subtract Report Time Min From STD
        $this->reportTime = Add_Minutes_To_DateTime($this->flightArrivalTime, $this->reportTimeMin, null, true );
    }

    public function setReleaseTime(){
        if (!$this->reportTime){
            return;
        }

        // Add Duration To Report Date Time
        $this->releaseTime = Add_Minutes_To_DateTime($this->reportTime, $this->serviceDuration);
    }


}