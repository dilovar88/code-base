<?php namespace App\Classes\Staff\Roster\ServiceTypes;

use App\Classes\Staff\Roster\FlightHandler;
use App\Models\AirlineService;

/**
 * Created by PhpStorm.
 */

class DepartureService extends Service implements IService
{

    /**
     * @var FlightHandler $flightHandler
     * @var AirlineService $serviceRecord
     * @var $serviceLicenseRequirements
     */
    public function __construct($flightHandler, $serviceRecord, $serviceLicenseRequirements){

        parent::__construct($flightHandler, $serviceRecord, $serviceLicenseRequirements);

        $this->setParameters($serviceRecord);
    }

    public function setParameters($serviceRecord){

        if (!$serviceRecord){
            return;
        }

        if ($serviceRecord->service_timings && $serviceRecord->service){
            $staffReq       = $serviceRecord->service->dep_staff_req;
            $staffMin       = $serviceRecord->service->dep_staff_min;
            $reportTimeMin  = $serviceRecord->service->dep_report_time;
            $releaseTimeMin = $serviceRecord->service->dep_release_time;
        }
        else {
            $staffReq       = $serviceRecord->dep_staff_req;
            $staffMin       = $serviceRecord->dep_staff_min;
            $reportTimeMin  = $serviceRecord->dep_report_time;
            $releaseTimeMin = $serviceRecord->dep_release_time;
        }

        $this->setStaffReq($staffReq ? $staffReq : $staffMin);

        $this->setStaffMin($staffMin ? $staffMin : $staffReq);

        $this->setReportTimeMin($reportTimeMin);

        $this->setReleaseTimeMin($releaseTimeMin);

        $this->setFlightDepartureTime();

        $this->setDuration();

        $this->setReportTime();

        $this->setReleaseTime();

    }

    public function setFlightDepartureTime(){
        if ($this->flight->ptd && $this->flight->ptd != EMPTY_DATETIME){
            $this->flightDepartureTime = $this->flight->ptd;
        }
        else if ($this->flight->std && $this->flight->std != EMPTY_DATETIME){
            $this->flightDepartureTime = $this->flight->std;
        }
        else if ($this->flight->atd && $this->flight->atd != EMPTY_DATETIME){
            $this->flightDepartureTime = $this->flight->atd;
        }
        else if ($this->flight->etd && $this->flight->etd != EMPTY_DATETIME){
            $this->flightDepartureTime = $this->flight->etd;
        }
    }

    public function setDuration(){
        if (!$this->serviceType){
            return;
        }

        $this->serviceDuration = $this->reportTimeMin - $this->releaseTimeMin;
    }

    public function setReportTime(){
        if (!$this->flightDepartureTime){
            return;
        }

        // Subtract Report Time Min From STD
        $this->reportTime = Add_Minutes_To_DateTime($this->flightDepartureTime, $this->reportTimeMin, null, true );
    }

    public function setReleaseTime(){
        if (!$this->reportTime){
            return;
        }

        // Add Duration To Report Date Time
        $this->releaseTime = Add_Minutes_To_DateTime($this->reportTime, $this->serviceDuration);
    }


}