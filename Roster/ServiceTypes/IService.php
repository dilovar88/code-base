<?php

namespace App\Classes\Staff\Roster\ServiceTypes;

interface IService
{
    public function setParameters($functionRecord);

    public function setDuration();

    public function getFlightDepartureTime();

    public function getFlightArrivalTime();
}