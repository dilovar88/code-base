<?php

namespace App\Classes\Staff\Roster\ServiceTypes;

use App\Classes\Staff\Roster\FlightHandler;
use App\Classes\Staff\Roster\Models\AirlineLicenses;
use App\Classes\Staff\Roster\Shift\Job;
use App\Models\AirlineService;
use App\Models\Flight;
use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $table = "staff_roster__services";

    public $timestamps = true;

    public function service(){
        return $this->belongsTo(\App\Models\Service::class);
    }

    public function licensesDB(){
        return $this->hasMany(AirlineLicenses::class, "staff_roster_service_id");
    }

    public function jobsDB(){
        return $this->hasMany(Job::class, "staff_roster_service_id");
    }

    public function serviceDB(){
        return $this->belongsTo(\App\Models\Service::class, "service_id");
    }

    // EXCLUDED FROM DB
    protected $flightHandler;

    protected $flightDepartureTime;

    protected $flightArrivalTime;

    protected $flightArrivalActualTime;

    // [license_id1, license_id2, ...]
    protected $licenses;

    // Service Type
    protected $serviceName;

    // IN DB

    /* @var Flight $flight */
    protected $flight;

    protected $serviceId;

    // Departure/Arrival
    protected $serviceType;

    // Man Power Required
    protected $staffReq;

    // Man Power Minimum
    protected $staffMin;

    // Report Time(DateTime)
    protected $reportTime;

    // Report time(minutes) BEFORE Departure / Arrival
    protected $reportTimeMin;

    // Release Time(DateTime)
    protected $releaseTime;

    // Release time(minutes) BEFORE Departure / AFTER Arrival
    protected $releaseTimeMin;

    // Duration(minutes) Calculated Based on Service Type
    protected $serviceDuration;

    /* @var Job[] $jobList*/
    protected $jobsList = [];

    /**
     * @var FlightHandler $flightHandler
     * @var AirlineService $serviceRecord
     * @var $serviceLicenseRequirements
     */
    public function __construct($flightHandler = null, $serviceRecord = null, $licenses = null){

        parent::__construct();

        if ($flightHandler && $serviceRecord) {

            $this->flightHandler = $flightHandler;

            $this->serviceType = $flightHandler->getServiceType();

            if (!$this->serviceType || !$flightHandler || !$serviceRecord || !in_array($this->serviceType, [DEPARTURE_SERVICE, ARRIVAL_SERVICE])) {
                return;
            }

            $this->setServiceTypeAndName($serviceRecord);

            $this->setFlight($flightHandler->getFlight());

            $this->licenses = $licenses;
        }
    }

    /**
     * @return mixed
     */
    public function getFlightDepartureTime()
    {
        return $this->flightDepartureTime;
    }

    /**
     * @return mixed
     */
    public function getFlightArrivalTime()
    {
        return $this->flightArrivalTime;
    }

    public function saveResult()
    {
        // Save
        $this->flight_handler_id = $this->flightHandler->id;
        $this->flight_id = $this->flight->id;
        $this->service_id = $this->serviceId;
        $this->service_type = $this->serviceType;
        $this->staff_req = $this->staffReq;
        $this->staff_min = $this->staffMin;
        $this->report_time = $this->reportTime;
        $this->report_time_min = $this->reportTimeMin;
        $this->release_time = $this->releaseTime;
        $this->release_time_min = $this->releaseTimeMin;
        $this->duration = $this->serviceDuration;
        $this->save();
    }

    /* @var FlightHandler $flightHandler */
    public function loadFromDB($flightHandler, $serviceArray)
    {
        // Load
        $this->flightHandler = $flightHandler;
        $this->flight = $flightHandler->getFlight();

        $this->serviceName = isset($serviceArray[$this->service_id]) ? $serviceArray[$this->service_id]['abbr'] : null;
        $this->serviceId = $this->service_id;
        $this->serviceType = $this->service_type;
        $this->staffReq = $this->staff_req;
        $this->staffMin = $this->staff_min;
        $this->reportTime = $this->report_time;
        $this->reportTimeMin = $this->report_time_min;
        $this->releaseTime = $this->release_time;
        $this->releaseTimeMin = $this->release_time_min;
        $this->serviceDuration = $this->duration;
    }


    public function scheduledJobCount(){
        return count($this->jobsList);
    }

    public function setServiceTypeAndName($serviceRecord){

        if (!$serviceRecord){
            return;
        }

        $this->serviceId = $serviceRecord->service_id;

        $this->serviceName = $serviceRecord->service ? $serviceRecord->service->abbr : "";

    }

    /**
     * @var \App\Classes\Staff\Roster\Shift\Job $job
     * @param null $k
     */
    public function addJob($job, $k = null){
        if ($k){
            array_splice($this->jobsList, $k, 0, [$job]);
        }
        else {
            $this->jobsList[] = $job;
        }
    }

    public function setFlight($var){
        $this->flight = $var;
    }

    public function setServiceName($var){
        $this->serviceName = $var;
    }

    public function setServiceType($var){
        $this->serviceType = $var;
    }

    public function setStaffReq($var){
        $this->staffReq = $var;
    }

    public function setStaffMin($var){
        $this->staffMin = $var;
    }

    public function setReportTimeMin($var){
        $this->reportTimeMin = $var;
    }

    public function setReleaseTimeMin($var){
        $this->releaseTimeMin = $var;
    }

    public function setJobsList($value){
        $this->jobsList = $value;
    }

    public function getJobsList(){
        return $this->jobsList;
    }

    public function getStaffActual(){
       return count($this->jobsList);
    }

    public function getServiceName(){
        return $this->serviceName;
    }

    public function getLicenses(){
        return $this->licenses;
    }

    public function getServiceId(){
        return $this->serviceId;
    }

    public function getServiceType(){
        return $this->serviceType;
    }

    public function getStaffReq(){
        return $this->staffReq;
    }

    public function getStaffMin(){
        return $this->staffMin;
    }

    public function getReportTime(){
        return $this->reportTime;
    }

    public function getReleaseTime(){
        return $this->releaseTime;
    }

    public function getReportTimeTimezone(){
        if ($this->flightHandler->getRoster()->timing == LOCAL){
            return utcToLocal($this->flightHandler->getRoster()->airportTimezones, $this->flightHandler->getStation()->id, $this->reportTime);
        }

        return $this->reportTime;
    }

    public function getReleaseTimeTimezone(){
        if ($this->flightHandler->getRoster()->timing == LOCAL){
            return utcToLocal($this->flightHandler->getRoster()->airportTimezones, $this->flightHandler->getStation()->id, $this->releaseTime);
        }

        return $this->releaseTime;
    }

    public function getReportDate(){
        return date("Y-m-d", strtotime($this->reportTime));
    }

    public function getReleaseDate(){
        return date("Y-m-d", strtotime($this->releaseTime));
    }

    public function getDuration($hour = false){
        if ($hour)
            return round($this->serviceDuration / 60, 4);

        return $this->serviceDuration;
    }

    public function setLicenses($value){
        return $this->licenses = $value;
    }

    public function printOutJobsDetails(){

        echo "Assigned: <strong>".count($this->jobsList)."</strong><br/>";
        foreach ($this->jobsList as $i => $job) {

            $user = $job->getShift()->getStaff()->getUser();
            echo ($i + 1). ") ".$user->last_name." ".$user->first_name."<br/>";
        }
        echo "<br/>";
    }

}