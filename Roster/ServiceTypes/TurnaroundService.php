<?php namespace App\Classes\Staff\Roster\ServiceTypes;

use App\Classes\Staff\Roster\FlightHandler;
use App\Models\AirlineService;

/**
 * Created by PhpStorm.
 */
class TurnaroundService extends Service implements IService
{
    protected $parentFlight;

    /**
     * @var FlightHandler $flightHandler
     * @var AirlineService $serviceRecord
     * @var $serviceLicenseRequirements
     */
    public function __construct($flightHandler, $serviceRecord, $serviceLicenseRequirements){

        parent::__construct($flightHandler, $serviceRecord, $serviceLicenseRequirements);

        $this->setParameters($serviceRecord);
    }

    public function setParameters($serviceRecord){

        if (!$serviceRecord){
            return;
        }
        $this->parentFlight = getParentFlight($this->flight);

        if ($serviceRecord->service_timings && $serviceRecord->service){
            $staffReq       = $serviceRecord->service->turn_staff_req;
            $staffMin       = $serviceRecord->service->turn_staff_min;
            $reportTimeMin  = $serviceRecord->service->turn_report_time;
            $releaseTimeMin = $serviceRecord->service->turn_release_time;
        }
        else {
            $staffReq       = $serviceRecord->turn_staff_req;
            $staffMin       = $serviceRecord->turn_staff_min;
            $reportTimeMin  = $serviceRecord->turn_report_time;
            $releaseTimeMin = $serviceRecord->turn_release_time;
        }

        $this->setStaffReq($staffReq ? $staffReq : $staffMin);

        $this->setStaffMin($staffMin ? $staffMin : $staffReq);

        $this->setReportTimeMin($reportTimeMin);

        $this->setReleaseTimeMin($releaseTimeMin);

        $this->setFlightDepartureTime();

        $this->setFlightArrivalTime();

        $this->setDuration();

        $this->setReportTime();

        $this->setReleaseTime();
    }

    public function setFlightArrivalTime(){
        if (!$this->parentFlight){
            return;
        }

        if ($this->parentFlight->pta && $this->parentFlight->pta != EMPTY_DATETIME){
            $this->flightArrivalTime = $this->parentFlight->pta;
        }
        elseif ($this->parentFlight->sta && $this->parentFlight->sta != EMPTY_DATETIME){
            $this->flightArrivalTime = $this->parentFlight->sta;
        }
        elseif ($this->parentFlight->ata && $this->parentFlight->ata != EMPTY_DATETIME){
            $this->flightArrivalTime = $this->parentFlight->ata;
        }
        elseif ($this->parentFlight->eta && $this->parentFlight->eta != EMPTY_DATETIME){
            $this->flightArrivalTime = $this->parentFlight->eta;
        }
    }

    public function setFlightDepartureTime(){
        if ($this->flight->ptd && $this->flight->ptd != EMPTY_DATETIME){
            $this->flightDepartureTime = $this->flight->ptd;
        }
        else if ($this->flight->std && $this->flight->std != EMPTY_DATETIME){
            $this->flightDepartureTime = $this->flight->std;
        }
        else if ($this->flight->atd && $this->flight->atd != EMPTY_DATETIME){
            $this->flightDepartureTime = $this->flight->atd;
        }
        else if ($this->flight->etd && $this->flight->etd != EMPTY_DATETIME){
            $this->flightDepartureTime = $this->flight->etd;
        }
    }

    public function setDuration(){
        if (!$this->serviceType){
            return;
        }

        $this->serviceDuration = $this->reportTimeMin + $this->releaseTimeMin;
    }

    /**

    $arr = getFlightArrivalInitialDatePTA(getParentFlight($flight));
    $dep = getFlightDepartureInitialDatePTD($flight);

    $plannedReport = date("Y-m-d H:i", strtotime($arr) - (60 * $obj->turn_report_time));
    $plannedRelease = date("Y-m-d H:i", strtotime($dep) - (60 * $obj->turn_release_time));
    $duration = date("H:i", strtotime($plannedRelease) - strtotime($plannedReport));

     */

    public function setReportTime(){
        if (!$this->flightArrivalTime){
            return;
        }

        // Subtract Report Time Min From STD
        $this->reportTime = Add_Minutes_To_DateTime($this->flightArrivalTime, $this->reportTimeMin, null, true );
    }

    public function setReleaseTime(){
        if (!$this->reportTime){
            return;
        }

        // Add Duration To Report Date Time
        $this->releaseTime = Add_Minutes_To_DateTime($this->flightDepartureTime, $this->releaseTimeMin, null, trans());
    }


}