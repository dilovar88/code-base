<?php

namespace App\Classes\Staff\Roster;

use App\Classes\Staff\Roster\Models\Airlines;
use App\Classes\Staff\Roster\Models\AirlineLicenses;
use App\Classes\Staff\Roster\Models\StaffLicenses;
use App\Classes\Staff\Roster\Models\StaffServices;
use App\Classes\Staff\Roster\ServiceTypes\Service;
use App\Classes\Staff\Roster\Settings\RosterSettings;
use App\Classes\Staff\Roster\Shift\Job;
use App\Classes\Staff\Roster\Shift\Shift;
use App\Classes\Staff\Roster\Staff\Staff;
use App\Mail\TemplateEmail;
use App\Models\Airport;
use App\Models\Flight;
use App\Models\FlightStaff;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class RosterOperations
{
    /* @var Roster $roster*/
    protected $roster;

    public function __construct($roster){
        $this->roster = $roster;

    }

    public function saveRoster($status = SAVED){

        // Roster
        try {
            $this->roster->status = $status;
            $this->roster->from = $this->roster->getFromDate();
            $this->roster->to = $this->roster->getToDate();
            $this->roster->airport_id = $this->roster->getAirport();
            $this->roster->created_by = Auth::user()->id;
            $this->roster->save();
        }
        catch (\Exception $e){
            debug($e->getMessage());
        }


        // Airlines
        try {
            if ($this->roster->getAirlines()) {
                foreach ($this->roster->getAirlines() as $each) {
                    $airline = new Airlines();
                    $airline->roster_id = $this->roster->id;
                    $airline->airline_id = $each;
                    $airline->save();
                }
            }
        }
        catch (\Exception $e){
            debug($e->getMessage());
        }


        // Flight Handlers
        /* @var FlightHandler $flightHandler */
        foreach ($this->roster->getFlightHandlers() as $flightHandler) {

            try {
                $flightHandler->saveResult();
            }
            catch (\Exception $e){
                debug($e->getMessage());
            }

            // Service
            /* @var \App\Classes\Staff\Roster\ServiceTypes\Service $service */
            foreach ($flightHandler->getServiceList() as $service) {

                try {
                    $service->saveResult();
                }
                catch (\Exception $e){
                    debug($e->getMessage());
                }


                // Airline License
                try {
                    if ($service->getLicenses()) {
                        foreach ($service->getLicenses() as $each) {
                            $airlineLicenses = new AirlineLicenses();
                            $airlineLicenses->airline_id = $flightHandler->getAirline()->id;
                            $airlineLicenses->staff_roster_service_id = $service->id;
                            $airlineLicenses->service_id = $service->service_id;
                            $airlineLicenses->license_id = $each;
                            $airlineLicenses->save();
                        }
                    }
                }
                catch (\Exception $e){
                    debug($e->getMessage());
                }
            }
        }

        // Staff
        if ($this->roster->getStaffList() && count($this->roster->getStaffList())){
            foreach ($this->roster->getStaffList() as $staff) {

                try{
                    $staff->saveResult($this->roster);
                }
                catch (\Exception $e){
                    debug($e->getMessage());
                }


                // Staff Service
                try {
                    foreach ($staff->getServicesList() as $airlineId => $serviceArray) {
                        foreach ($serviceArray as $each) {
                            $staffServices = new StaffServices();
                            $staffServices->staff_roster_staff_id = $staff->id;
                            $staffServices->user_id = $staff->getUser()->id;
                            $staffServices->airline_id = $airlineId;
                            $staffServices->service_id = $each;
                            $staffServices->save();
                        }
                    }
                }
                catch (\Exception $e){
                    debug($e->getMessage());
                }


                // Staff License
                try {
                    if ($staff->getValidation()->getLicenses()) {
                        foreach ($staff->getValidation()->getLicenses() as $licenseId => $expirationDate) {
                            $staffLicenses = new StaffLicenses();
                            $staffLicenses->staff_roster_staff_id = $staff->id;
                            $staffLicenses->user_id = $staff->getUser()->id;
                            $staffLicenses->license_id = $licenseId;
                            $staffLicenses->expiration_date = $expirationDate;
                            $staffLicenses->save();
                        }
                    }
                }
                catch (\Exception $e){
                    debug($e->getMessage());
                }


                // Shift
                foreach ($staff->getShiftsList() as $shift) {
                    try {
                        $shift->saveResult();
                    }
                    catch (\Exception $e){
                        debug($e->getMessage());
                    }

                    // Job
                    foreach ($shift->getJobsList() as $job) {
                        try {
                            $job->saveResult();
                        }
                        catch (\Exception $e){
                            debug($e->getMessage());
                        }
                    }
                }
            }
        }

        // Roster Settings
        try {
            $settings = $this->roster->getSettings();
            $settings->saveResult();
        }
        catch (\Exception $e){
            debug($e->getMessage());
        }

    }

    public function loadRoster(){

        $flightIDs = FlightHandler::where("roster_id", $this->roster->id)
                                        ->pluck("flight_id")
                                        ->all();

        $flightsByIDs = Flight::whereIn("id", $flightIDs)
                                ->with([
                                    "flightNumber",
                                    "flightNumber.airline",
                                    "flightNumber.departureAirport",
                                    "flightNumber.arrivalAirport",
                                ])
                                ->get()
                                ->keyBy("id");

        // Roster
        $this->roster->setFromDate($this->roster->from);
        $this->roster->setToDate($this->roster->to);
        $this->roster->setAirport($this->roster->airport_id);

        // Airlines
        $airlines = $this->roster->airlinesDB;
        $this->roster->setAirlines($airlines->pluck("airline_id"));

        /* @var FlightHandler[] $flightHandlers*/
        $flightHandlers = $this->roster->flightHandlersDB;
        $this->roster->setFlightHandlers($flightHandlers);

        // Roster Settings Initial From DB
        /* @var RosterSettings $settings*/
        $settings = $this->roster->settingsDB;
        $settings->loadInitialPropertiesFromDB($this->roster);

        // Others
        $usersArray = makeObjectIdAsArrayKey(User::with(["location", "location.airport"])->get());
        $airportsArray = Airport::getHandlingStationsIdAndObject();
        $serviceArray = makeObjectIdAsArrayKey(\App\Models\Service::get());

        $flightHandlerArray = [];
        $staffRosterServicesArray = [];

        $staffArray = [];
        $shiftsArray = [];

        foreach ($flightHandlers as &$flightHandler) {
            $flightHandler->loadFromDB($this->roster, $flightsByIDs, $airportsArray);

            /* @var Service[] $services*/
            $services = $flightHandler->servicesDB;

            // Service
            foreach ($services as &$service) {

                $service->loadFromDB($flightHandler, $serviceArray);

                // Jobs
                /* @var Job[] $jobs*/
                $jobs = $service->jobsDB;

                foreach ($jobs as &$job) {

                    // Shift (find)
                    /* @var Shift $shift*/
                    $shift = $job->shiftDB;
                    $shift->settings = $settings;

                    $shiftExists = false;

                    if (array_key_exists($shift->id, $shiftsArray)){
                        $shiftExists = true;
                        $shift = $shiftsArray[$shift->id];
                    }

                    // Staff (find & Load)
                    /* @var Staff $staff */
                    $staff = $shift->staffDB;

                    if (array_key_exists($staff->id, $staffArray)){
                        $staff = $staffArray[$staff->id];
                    }
                    else {
                        // Staff Licenses
                        $licenses = $staff->licensesDB;
                        $licenses = $licenses ? $licenses->pluck("expiration_date", "license_id") : null;

                        $staff->loadFromDB($this->roster, $usersArray, $airportsArray, $licenses);

                        $this->roster->addStaffToStaffList($staff);

                        $staffArray[$staff->id] = $staff;
                    }


                    // Shift (Load)
                    if (!$shiftExists){
                        $shift->loadFromDB($settings, $staff);
                        $shiftsArray[$shift->id] = $shift;
                        $staff->addShiftToShiftsList($shift);
                    }

                    // Job (Load)
                    $job->loadFromDB($shift, $flightHandler, $service);

                    // Shift (Fill)
                    $shift->addJobToJobsList($job);
                }
                $service->setJobsList($jobs);

                // Service License
                $licenses = $service->licensesDB;
                $service->setLicenses($licenses->pluck("license_id"));

                $staffRosterServicesArray[$service->id] = $service;
            }

            $flightHandler->setServiceList($services);
            // Array
            $flightHandlerArray[$flightHandler->id] = $flightHandler;
        }

        // Roster Settings Assigned Flights From DB
        $settings->loadAssignedFlightsFromDB($this->roster, $flightHandlers, $staffRosterServicesArray, $staffArray);
        $this->roster->setSettings($settings);
    }

    public function publishRoster(){
        // Set
        $this->roster->setFromDate($this->roster->from);
        $this->roster->setToDate($this->roster->to);
        $this->roster->setAirport($this->roster->airport_id);

        // Airlines
        $airlines = $this->roster->airlinesDB;
        $this->roster->setAirlines($airlines->pluck("airline_id"));
        // --------------


        /* @var Staff[] $staffList*/
        $staffList = $this->roster->staffDB;

        $now = date("Y-m-d H:i:s");
        $strToday = strtotime(date("Y-m-d"));
        $insertArray = $existingFlightStaffIDs = [];
        foreach ($staffList as $staff) {

            /* @var Shift[] $shifts*/
            $shifts = $staff->shiftsDB;

            foreach ($shifts as $shift) {

                /* @var Job[] $jobs*/
                $jobs = $shift->jobsDB;

                foreach ($jobs as $job) {

                    if ($job->staff_type == MANUALLY_ROSTERED_TYPE){
                        continue;
                    }

                    // Flight Handler
                    /* @var FlightHandler $flightHandler*/
                    $flightHandler = $job->flightHandlerDB;

                    /* @var Service $service*/
                    $service = $job->serviceDB;

                    $fStaff = FlightStaff::where("flight_id", $flightHandler->flight_id)
                                            ->where("user_id", $staff->user_id)
                                            ->where("service_id", $service->service_id)
                                            ->whereNull("deleted_at")
                                            ->first();

                    if ($fStaff){
                        $existingFlightStaffIDs[] = $fStaff->id;
                    }
                    else {
                        $insertArray[] = [
                            'flight_id'             => $flightHandler->flight_id,
                            'user_id'               => $staff->user_id,
                            'service_id'            => $service->service_id,

                            'staff_roster_id'       => $this->roster->id,
                            'service_type'          => $service->service_type,
                            'planned_report_time'   => $job->report_time,
                            'planned_release_time'  => $job->release_time,
                            'created_at'            => $now,
                            'created_by'            => Auth::user()->id,
                        ];
                    }
                }
            }

            try{
                $this->sendIndividualEmail($staff->getUser());
            }
            catch (\Exception $e){

            }
        }

        if (count($insertArray)){

            $periodFrom = strtotime($this->roster->from) > $strToday ? $this->roster->from : date("Y-m-d", strtotime("+ 1 days"));

            $flightIDs = FlightHandler::join("flights", "flights.id", "=", "staff_roster__flight_handlers.flight_id")
                        ->join("flights__numbers", "flights__numbers.id", "=", "flights.flight_number_id")
                        ->where("roster_id", $this->roster->id)
                        ->where(function($sql){
                            $sql->where("flights__numbers.departure_airport_id", $this->roster->getAirport())
                                ->orWhere("flights__numbers.arrival_airport_id", $this->roster->getAirport());
                        })
                        ->where(DB::raw("DATE(ptd)"), ">=",  $periodFrom)
                        ->where(DB::raw("DATE(ptd)"), "<=",  $this->roster->to)
                        ->pluck("flights.id")
                        ->all();

            if (count($flightIDs)){
                $fStaff = FlightStaff::whereIn("flight_id", $flightIDs)
                            ->whereNotIn("id", $existingFlightStaffIDs)
                            ->get();

                // Audit Deleted
                auditDeleted($fStaff);
            }

            if (count($insertArray)){
                FlightStaff::insert($insertArray);
            }
        }

        $this->roster->status = PUBLISHED;
        $this->roster->save();
    }

    public function sendIndividualEmail($user){
        Mail::queue($user->email)
            ->send(new TemplateEmail(view("emails/notification/staff_roster_changed", [
                    "user"              => $user,
                ])->render()
            ,  "Staff Roster has been published/updated"));
    }

}
