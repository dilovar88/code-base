<?php


namespace App\Classes\Staff\Roster\Models;

use Illuminate\Database\Eloquent\Model;

class StaffLicenses extends Model
{
    protected $table = "staff_roster__staff_licenses";

    public $timestamps = true;
}
