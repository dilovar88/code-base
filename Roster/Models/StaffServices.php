<?php


namespace App\Classes\Staff\Roster\Models;

use Illuminate\Database\Eloquent\Model;

class StaffServices extends Model
{
    protected $table = "staff_roster__staff_services";

    public $timestamps = true;
}
