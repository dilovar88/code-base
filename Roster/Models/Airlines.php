<?php


namespace App\Classes\Staff\Roster\Models;

use Illuminate\Database\Eloquent\Model;

class Airlines extends Model
{
    protected $table = "staff_roster__airlines";

    public $timestamps = true;
}
