<?php


namespace App\Classes\Staff\Roster\Models;

use Illuminate\Database\Eloquent\Model;

class AirlineLicenses extends Model
{
    protected $table = "staff_roster__airline_licenses";

    public $timestamps = true;
}
