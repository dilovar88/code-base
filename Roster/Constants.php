<?php namespace App\Classes\Staff\Roster;


abstract class Constants
{
    const DAY_SHIFT         = "Day";
    const NIGHT_SHIFT       = "Night";

    const DEPARTURE_SERVICE    = "Departure Service";
    const ARRIVAL_SERVICE      = "Arrival Service";

    const SUPERVISION    	= 	"Supervision";
    const CHECK_IN  		=	"Check-in";
    const BOARDING   		=	"Boarding";
    const EB_COLLECTION    	=	"Ex Bag";
    const TICKETING    		=	"Ticketing";
    const RAMP    			=	"Ramp";
    const WALK_OUT 			=	"Walk Out";
    const LOST_AND_FOUND   	=	"Lost and Found";
    const SECURITY_SEARCH   =	"Security Search";
    const SECURITY_GUARDING =	"Security Guarding";
    const DOCUMENT_CHECK    =	"Document Check";
    const ARRIVAL_SERVICES  =	"Arrival Services";
    const MEET_AND_ASSIST   =	"Meet and Assist";
    const AIRLINE_ADMIN     =	"Airline Admin";
    const LOAD_CONTROL      =  	"Load Control";

}
