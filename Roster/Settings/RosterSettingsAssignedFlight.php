<?php
/**
 * Created by PhpStorm.
 */

namespace App\Classes\Staff\Roster\Settings;

use App\Classes\Staff\Roster\FlightHandler;
use App\Classes\Staff\Roster\ServiceTypes\Service;
use App\Classes\Staff\Roster\Staff\Staff;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class RosterSettingsAssignedFlight extends Model
{
    protected $table = "staff_roster__settings_assigned_flights";

    public $timestamps = true;

    public function rosterSettingsDB(){
        return $this->belongsTo(RosterSettings::class, "roster_settings_id");
    }

    public function flightHandlerDB(){
        return $this->belongsTo(FlightHandler::class, "flight_handler_id");
    }

    public function assignedStaffDB(){
        return $this->hasMany(RosterSettingsAssignedStaff::class, "assigned_flight_id");
    }

    public function assignedServicesDB(){
        return $this->hasMany(RosterSettingsAssignedService::class, "assigned_flight_id");
    }

    /* @var RosterSettings $rosterSettings */
    protected $rosterSettings;

    /* @var RosterSettingsAssignedService[] $assignedServices */
    protected $assignedServices = [];

    /* @var FlightHandler $flightHandler */
    protected $flightHandler;

    /**
     * @var RosterSettings $rosterSettings
     * @var FlightHandler $flightHandler
     * @param null $servicesAndStaffList
     * @param null $servicesAndStaffListFromDb
     */
    public function __construct($rosterSettings = null, $flightHandler = null, $servicesAndStaffList = null, $servicesAndStaffListFromDb = null){

        parent::__construct();

        $this->rosterSettings = $rosterSettings;

        $this->flightHandler = $flightHandler;

        if ($servicesAndStaffList){
            $this->addAssignedServicesAndStaff($servicesAndStaffList);
        }
        else if ($servicesAndStaffListFromDb){
            $this->addAssignedStaffFromDb($servicesAndStaffListFromDb);
        }
    }

    public function getRosterSettings(){
        return $this->rosterSettings;
    }

    public function getFlightHandler(){
        return $this->flightHandler;
    }

    public function getAssignedServices(){
        return $this->assignedServices;
    }

    public function addAssignedServicesAndStaff($servicesAndStaffList){
        if ($servicesAndStaffList && count($servicesAndStaffList)){
            foreach ($servicesAndStaffList as $serviceId => $userIds) {

                $rosterService = $this->flightHandler->getRosterServiceByServiceId($serviceId);

                if ($rosterService) {

                    $staffList = [];
                    foreach ($userIds as $eachId) {

                        $staff = $this->rosterSettings->getRoster()->getStaffObjectByUserId($eachId);

                        if ($staff) {
                            $staffList[] = $staff;
                        }
                    }

                    $this->assignedServices[] = new RosterSettingsAssignedService($this, $rosterService, $staffList);
                }
            }
        }
    }

    public function addAssignedStaffFromDb($servicesAndStaffList){
        $users = $services = [];
        if ($servicesAndStaffList && count($servicesAndStaffList)){
            foreach ($servicesAndStaffList as $each) {

                if (array_key_exists($each->service_id, $services)){
                    $service = $services[$each->service_id];
                }
                else {
                    $service = Service::find($each->service_id);
                    $services[$each->service_id] = $service;
                }

                if (array_key_exists($each->user_id, $users)){
                    $user = $users[$each->user_id];
                }
                else {
                    $user = User::find($each->user_id);
                    $users[$each->user_id] = $user;
                }

                $this->assignedServices[] = new RosterSettingsAssignedService($this, $service, $user);
            }
        }
    }

    public function saveResult($rosterSettings)
    {
        // Save
        $this->rosterSettings = $rosterSettings;

        $this->roster_settings_id = $this->rosterSettings->id;
        $this->flight_handler_id = $this->flightHandler->id;
        $this->save();

        foreach ($this->assignedServices as $assignedService) {
            $assignedService->saveResult($this);
        }

    }

    /**
     * @var FlightHandler[] $flightHandlers
     * @param $flightHandlerId
     * @return null
     */
    public function findFlightHandler($flightHandlers, $flightHandlerId){
        if ($flightHandlers && count($flightHandlers) && $flightHandlerId){
            foreach ($flightHandlers as $each) {
                if ($each->id == $flightHandlerId){
                    return $each;
                }
            }
        }

        return null;
    }

    /**
     * @var RosterSettings $rosterSettings
     * @var FlightHandler[] $flightHandlers
     * @var Service[] $staffRosterServicesArray
     * @var Staff[] $staffArray
     * @return $this
     */
    public function loadFromDB($rosterSettings, $flightHandlers, $staffRosterServicesArray, $staffArray)
    {
        // Load
        $this->rosterSettings = $rosterSettings;
        $this->flightHandler = $this->findFlightHandler($flightHandlers, $this->flight_handler_id);

        /* @var RosterSettingsAssignedService[] $assignedServices */
        $assignedServices = $this->assignedServicesDB;
        foreach ($assignedServices as $assignedService) {
            $this->assignedServices[] = $assignedService->loadFromDB($this, $staffRosterServicesArray, $staffArray);
        }

        return $this;
    }

}