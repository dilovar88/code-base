<?php
/**
 * Created by PhpStorm.
 */

namespace App\Classes\Staff\Roster\Settings;

use App\Classes\Staff\Roster\FlightHandler;
use App\Classes\Staff\Roster\Roster;
use App\Classes\Staff\Roster\ServiceTypes\Service;
use App\Classes\Staff\Roster\Staff\Staff;
use App\Models\Flight;
use Illuminate\Database\Eloquent\Model;

class RosterSettings extends Model
{
    protected $table = "staff_roster__settings";

    public $timestamps = true;

    public function rosterDB(){
        return $this->belongsTo(Roster::class, "roster_id");
    }

    public function assignedFlightsDB(){
        return $this->hasMany(RosterSettingsAssignedFlight::class, "roster_settings_id");
    }

    protected $shiftMaxHours;
    protected $shiftMinHours;
    protected $shiftMinRestTime;
    protected $staffAssignment;
    protected $airlineLicenseCheck;
    protected $airportLicenseCheck;

    /* @var Roster $roster */
    protected $roster;

    /* @var RosterSettingsAssignedFlight[] $assignedFlights*/
    protected $assignedFlights = [];

    public function __construct($roster = null){

        parent::__construct();

        $this->roster = $roster;
    }

    /**
     * @param Roster $roster
     */
    public function setRoster($roster)
    {
        $this->roster = $roster;
    }

    // GET
    public function getRoster(){
        return $this->roster;
    }

    public function getAssignedFlights(){
        return $this->assignedFlights;
    }

    public function getShiftMaxHours(){
        return $this->shiftMaxHours;
    }

    public function getShiftMinHours(){
        return $this->shiftMinHours;
    }

    public function getShiftMinRestTime(){
        return $this->shiftMinRestTime;
    }

    public function getStaffAssignment(){
        return $this->staffAssignment;
    }

    public function getAirlineLicenseCheck(){
        return $this->airlineLicenseCheck;
    }

    public function getAirportLicenseCheck(){
        return $this->airportLicenseCheck;
    }

    // SET
    public function setAssignedFlights($value){
        $this->assignedFlights = $value;
    }

    public function setShiftMaxHours($value){
        $this->shiftMaxHours = $value;
    }

    public function setShiftMinHours($value){
        $this->shiftMinHours = $value;
    }

    public function setShiftMinRestTime($value){
        $this->shiftMinRestTime = $value;
    }

    public function setStaffAssignment($value){
        $this->staffAssignment = $value;
    }

    public function setAirlineLicenseCheck($value){
        $this->airlineLicenseCheck = $value ? true : false;
    }

    public function setAirportLicenseCheck($value){
        $this->airportLicenseCheck = $value ? true : false;
    }

    public function addAssignedFlight($flightId, $servicesAndStaffList = null, $servicesAndStaffListFromDb = null){

        $flight = Flight::find($flightId);
        // Initiate Flight Handler
        $flightHandler = new FlightHandler($this->getRoster(), $flight, ASSIGNED_TYPE);

        $this->roster->addFlightHandler($flightHandler);

        $this->assignedFlights[] = new RosterSettingsAssignedFlight($this, $flightHandler, $servicesAndStaffList, $servicesAndStaffListFromDb);
    }

    public function saveResult()
    {
        // Save
        $this->roster_id = $this->roster->id;
        $this->shift_max_hours = $this->shiftMaxHours;
        $this->shift_min_hours = $this->shiftMinHours;
        $this->shift_min_rest_time = $this->shiftMinRestTime;
        $this->staff_assignment = $this->staffAssignment;
        $this->airline_license_check = $this->airlineLicenseCheck;
        $this->airport_license_check = $this->airportLicenseCheck;
        $this->save();

        // Save Assigned Flight (Inside saves Assigned Staff)
        foreach ($this->assignedFlights as $assignedFlight)
        {
            $assignedFlight->saveResult($this);
        }
    }

    /* @var Roster $roster */
    public function loadInitialPropertiesFromDB($roster)
    {
        // Load
        $this->roster = $roster;

        $this->shiftMaxHours = $this->shift_max_hours;
        $this->shiftMinHours = $this->shift_min_hours;
        $this->shiftMinRestTime = $this->shift_min_rest_time;
        $this->staffAssignment = $this->staff_assignment;
        $this->airlineLicenseCheck = $this->airline_license_check;
        $this->airportLicenseCheck = $this->airport_license_check;

    }

    /**
     * @var Roster $roster
     * @var FlightHandler[] $flightHandlers
     * @var Service[] $staffRosterServicesArray
     * @var Staff[] $staffArray
     */
    public function loadAssignedFlightsFromDB($roster, $flightHandlers, $staffRosterServicesArray, $staffArray)
    {
        // Load
        $this->roster = $roster;

        /* @var RosterSettingsAssignedFlight[] $assignedFlights*/
        $assignedFlights = $this->assignedFlightsDB;
        foreach ($assignedFlights as $assignedFlight) {
            $this->assignedFlights[] = $assignedFlight->loadFromDB($this, $flightHandlers, $staffRosterServicesArray, $staffArray);
        }
    }

}