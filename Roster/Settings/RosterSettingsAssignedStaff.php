<?php
/**
 * Created by PhpStorm.
 */

namespace App\Classes\Staff\Roster\Settings;

use App\Classes\Staff\Roster\Staff\Staff;
use Illuminate\Database\Eloquent\Model;

class RosterSettingsAssignedStaff extends Model
{
    protected $table = "staff_roster__settings_assigned_staff";

    public $timestamps = true;

    public function assignedServiceDB(){
        return $this->belongsTo(RosterSettingsAssignedService::class, "assigned_service_id");
    }

    public function staffRosterStaffDB(){
        return $this->belongsTo(Staff::class, "staff_roster_staff_id");
    }

    /* @var RosterSettingsAssignedService $assignedService */
    protected $assignedService;

    /* @var Staff $staff */
    protected $staff;

    /**
     * @var RosterSettingsAssignedService $assignedService
     * @var Staff $staff
    */
    public function __construct($assignedService = null, $staff = null){

        parent::__construct();

        $this->assignedService = $assignedService;
        $this->staff = $staff;
    }

    public function getAssignedService(){
        return $this->assignedService;
    }

    public function getStaff(){
        return $this->staff;
    }

    /**
     * @var Staff[] $staffArray
     * @param $staffRosterStaffId
     * @return null
     */
    public function findStaff($staffArray, $staffRosterStaffId){
        if ($staffArray && count($staffArray) && $staffRosterStaffId){
            foreach ($staffArray as $each) {
                if ($each->id == $staffRosterStaffId){
                    return $each;
                }
            }
        }

        return null;
    }

    /* @var RosterSettingsAssignedService $assignedService */
    public function saveResult($assignedService)
    {
        $this->assignedService = $assignedService;

        // Save
        $this->assigned_service_id = $this->assignedService->id;
        $this->staff_roster_staff_id = $this->staff->id;
        $this->save();
    }

    /**
     * @param $assignedService
     * @var Staff[] $staffArray
     * @return $this
     */
    public function loadFromDB($assignedService, $staffArray)
    {
        // Load
        $this->assignedService = $assignedService;
        $this->staff = $this->findStaff($staffArray, $this->staff_roster_staff_id);

        return $this;
    }

}