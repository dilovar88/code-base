<?php
/**
 * Created by PhpStorm.
 */

namespace App\Classes\Staff\Roster\Settings;

use App\Classes\Staff\Roster\ServiceTypes\Service;
use App\Classes\Staff\Roster\Staff\Staff;
use Illuminate\Database\Eloquent\Model;

class RosterSettingsAssignedService extends Model
{
    protected $table = "staff_roster__settings_assigned_services";

    public $timestamps = true;

    public function assignedFlightDB(){
        return $this->belongsTo(RosterSettingsAssignedFlight::class, "assigned_flight_id");
    }

    public function assignedStaffDB(){
        return $this->hasMany(RosterSettingsAssignedStaff::class, "assigned_service_id");
    }

    public function staffRosterServiceDB(){
        return $this->belongsTo(Service::class, "staff_roster_service_id");
    }

    /* @var RosterSettingsAssignedFlight $assignedFlight*/
    protected $assignedFlight;

    /* @var Service $staffRosterService */
    protected $staffRosterService;

    /* @var RosterSettingsAssignedStaff[] $assignedStaff */
    protected $assignedStaff = [];

    /**
     * @var RosterSettingsAssignedFlight $assignedFlight
     * @var Service $staffRosterService
     * @var Staff[] $staffList
     */
    public function __construct($assignedFlight = null, $staffRosterService = null, $staffList = null){

        parent::__construct();

        $this->assignedFlight = $assignedFlight;
        $this->staffRosterService = $staffRosterService;

        if ($staffList && count($staffList)){
            foreach ($staffList as $staff) {
                $this->assignedStaff[] = new RosterSettingsAssignedStaff($this, $staff);
            }
        }

    }

    public function getAssignedFlight(){
        return $this->assignedFlight;
    }

    public function getAssignedStaff(){
        return $this->assignedStaff;
    }

    public function getStaffRosterService(){
        return $this->staffRosterService;
    }

    /* @var RosterSettingsAssignedFlight $assignedFlight */
    public function saveResult($assignedFlight)
    {
        $this->assignedFlight = $assignedFlight;

        // Save Current Roster
        $this->assigned_flight_id = $this->assignedFlight->id;
        $this->staff_roster_service_id = $this->staffRosterService->id;
        $this->save();

        foreach ($this->assignedStaff as $assignedStaff) {
            $assignedStaff->saveResult($this);
        }

    }

    /**
     * @var Service[] $staffRosterServicesArray
     * @param $staffRosterServiceId
     * @return null
     */
    public function findStaffRosterService($staffRosterServicesArray, $staffRosterServiceId){
        if ($staffRosterServicesArray && count($staffRosterServicesArray) && $staffRosterServiceId){
            foreach ($staffRosterServicesArray as $each) {
                if ($each->id == $staffRosterServiceId){
                    return $each;
                }
            }
        }

        return null;
    }

    /**
     * @param $assignedFlight
     * @var Service[] $staffRosterServicesArray
     * @var Staff[] $staffArray
     * @return $this
     */
    public function loadFromDB($assignedFlight, $staffRosterServicesArray, $staffArray)
    {
        // Load
        $this->assignedFlight = $assignedFlight;
        $this->staffRosterService = $this->findStaffRosterService($staffRosterServicesArray, $this->staff_roster_service_id);

        /* @var RosterSettingsAssignedStaff[] $assignedStaff */
        $assignedStaff = $this->assignedStaffDB;
        foreach ($assignedStaff as $each) {
            $this->assignedStaff[] = $each->loadFromDB($this, $staffArray);
        }

        return $this;
    }

}