<?php
/**
 * Created by PhpStorm.
 */

namespace App\Classes\Staff\Roster;

use App\Classes\Staff\Roster\ServiceTypes\Service;
use app\Classes\Staff\Roster\Shift\Shift;
use App\Models\AirlineLicense;
use App\Models\AirportLicense;
use App\Models\StaffLicense;
use App\Models\StaffService;
use App\Models\User;
use App\Models\UserHistory;
use App\Models\UserLeaveRequest;
use App\Models\UserPreferredOff;

class Helpers
{

    public static function getServicesList(){
        return \App\Models\Service::pluck("name", "id")->all();
    }

    public static function getAirportLicenseRequirements(){

        $airportLicenses = AirportLicense::where("enabled", 1)
                                        ->where("required", 1)
                                        ->get();

        $result = [];
        foreach ($airportLicenses as $each) {
            if (!isset($result[$each->airport_id])){
                $result[$each->airport_id] = [];
            }

            $result[$each->airport_id][] = $each->license_id;
        }

        return $result;
    }

    public static function getAirlineLicenseRequirements(){

        $airlineLicenses = AirlineLicense::where("enabled", 1)
                                            ->where("required", 1)
                                            ->get();

        $result = [];
        foreach ($airlineLicenses as $each) {
            if (!isset($result[$each->airline_id])){
                $result[$each->airline_id] = [];
            }
            if (!isset($result[$each->airline_id][$each->service_id])){
                $result[$each->airline_id][$each->service_id] = [];
            }

            $result[$each->airline_id][$each->service_id][] = $each->license_id;
        }

        return $result;
    }

    public static function getStaffLeaveRequests($userIDs, $from, $to){
        $list = [];

        $requests = UserLeaveRequest::with(["leave"])
                        ->whereIn("user_id", $userIDs)
                        ->whereNull("deleted_at")
                        ->where("status_id", 2)
                        ->where(function($sql) use($from, $to){
                            $sql->whereBetween("date_to", [$from, $to])
                                ->orWhereBetween("date_from", [$from, $to] )
                                ->orWhere(function ($sql2) use($from, $to){
                                    $sql2->where("date_from", "<=", $from)
                                        ->where("date_to", ">=", $to);
                                });
                        })
                        ->get();

        $history = UserHistory::with(["condition"])
                        ->whereIn("user_id", $userIDs)
                        ->whereNull("deleted_at")
                        ->where(function($sql) use($from, $to){
                            $sql->whereBetween("condition_to", [$from, $to])
                                ->orWhereBetween("condition_from", [$from, $to] )
                                ->orWhere(function ($sql2) use($from, $to){
                                    $sql2->where("condition_from", "<=", $from)
                                        ->where("condition_to", ">=", $to);
                                });
                        })
                        ->get();

        $preferredOffs = UserPreferredOff::whereIn("user_id", $userIDs)->get();
        $weekDaysArr = weekDaysArray(true, true);
        $offs = [ 1 => [], 2 => [], 3 => [], 4 => [], 5 => [], 6 => [], 0 => [], ];

        foreach ($preferredOffs as $each){
            foreach($weekDaysArr as $i => $day){
                if ($each->{"{$i}_from"} && $each->{"{$i}_to"}){
                    if (!isset($offs[$i][$each->user_id])){
                        $offs[$i][$each->user_id] = [];
                    }

                    $offs[$i][$each->user_id][$i] = [
                        $each->{"{$i}_from"},
                        $each->{"{$i}_to"},
                        (($each->{"{$i}_from"} == "00:00" || $each->{"{$i}_from"} == "00:00:00")
                        && ($each->{"{$i}_to"} == "23:59" || $each->{"{$i}_to"} == "23:59:00" || $each->{"{$i}_to"} == "23:59:59"))
                            ? "Day off" : "Off time"
                    ];
                }
            }
        }

        for($i = strtotime($from); $i < strtotime($to); $i += 24 * 60 * 60){
            $weekDay = date("w", $i);

            if (count($offs[$weekDay])){
                foreach ($offs[$weekDay] as $userID => $arr){
                    foreach ($arr as $period) {
                        $list[$userID][] = [
                            date("Y-m-d", $i) ." ". $period[0],
                            date("Y-m-d", $i) ." ". $period[1],
                            $period[2],
                            ""
                        ];
                    }
                }
            }
        }

        $offDays = User::whereIn("id", $userIDs)
                ->whereNotNull("roster_off_days")
                ->get();

        foreach ($offDays as $each) {
            if (!isset($list[$each->id])){
                $list[$each->id] = [];
            }

            $days = explode(";", $each->roster_off_days);
            for($i = strtotime($from); $i < strtotime($to); $i += 24 * 60 * 60){
                $weekDay = date("w", $i);
                if (in_array($weekDay, $days)){
                    $list[$each->id][] = [
                        date("Y-m-d", $i)." 00:00:00",
                        date("Y-m-d", $i)." 23:59:59",
                        "Day off",
                        ""
                    ];
                }
            }
        }

        foreach ($requests as $request) {
            if (!isset($list[$request->user_id])){
                $list[$request->user_id] = [];
            }

            $list[$request->user_id][] = [
                $request->date_from,
                $request->date_to,
                ($request->leave ? $request->leave->name : ""),
                $request->notes
            ];
        }

        foreach ($history as $request) {
            if (!isset($list[$request->user_id])){
                $list[$request->user_id] = [];
            }


            $list[$request->user_id][] = [
                $request->condition_from,
                $request->condition_to,
                ($request->condition ? $request->condition->name : ""),
                $request->notes
            ];
        }

        return $list;
    }

    public static function getAirlineLicensesByStaff($users){

        $serviceLicenses = StaffLicense::whereIn("user_id", $users)
                                        ->get();

        $result = [];
        foreach ($serviceLicenses as $each) {
            if (!isset($result[$each->user_id])){
                $result[$each->user_id] = [];
            }
            if (!isset($result[$each->user_id][$each->license_id])){
                $result[$each->user_id][$each->license_id] = null;
            }
            $result[$each->user_id][$each->license_id] = $each->expiry_date;
        }

        return $result;
    }

    public static function getUserIdsArray($users){
        $result = [];

        foreach ($users as $each) {
            $result[] = $each->id;
        }

        return $result;
    }


    /**
     * @var $shift Shift
     * @var $service Service
     * @return bool
     */
    public static function jobAndShiftTimingConflicts($shift, $service){
        $serviceStart = strtotime($service->getReportTime());
        $serviceEnd = strtotime($service->getReleaseTime());

        $shiftStart = strtotime($shift->getStartTime());
        $shiftEnd = strtotime($shift->getEndTime());

        if ($serviceStart <= $shiftStart){
            return true;
        }

        // If Job Start in Between
        if ($serviceStart >= $shiftStart && $serviceStart < $shiftEnd){
            return true;
        }

        // If Job End in Between
        if ($serviceEnd > $shiftStart && $serviceEnd <= $shiftEnd){
            return true;
        }

        // Job Starts Before Shift And Ends After Shift
        if ($serviceStart < $shiftStart && $serviceEnd >= $shiftEnd){
            return true;
        }

        return false;
    }

    /**
     * @var $shift Shift
     * @var $service Service
     * @return bool
     */
    public static function jobAndShiftTimingConflictsForAssigned($shift, $service){
        $serviceStart = strtotime($service->getReportTime());
        $serviceEnd = strtotime($service->getReleaseTime());

        $shiftStart = strtotime($shift->getStartTime());
        $shiftEnd = strtotime($shift->getEndTime());

        // If Job Start in Between
        if ($serviceStart >= $shiftStart && $serviceStart < $shiftEnd){
            return true;
        }

        // If Job End in Between
        if ($serviceEnd > $shiftStart && $serviceEnd <= $shiftEnd){
            return true;
        }

        // Job Starts Before Shift And Ends After Shift
        if ($serviceStart < $shiftStart && $serviceEnd >= $shiftEnd){
            return true;
        }

        return false;
    }


    public static function getServicesByStaff($userIds){

        $staffFunctions = StaffService::join("services", "services.id", "=", "staff__services.service_id")
                                    ->whereIn("user_id", $userIds)
                                    ->whereNotNull("airline_id")
                                    ->whereNotNull("service_id")
                                    ->orderBy("airline_id")
                                    ->orderBy("service_id")
                                    ->get([
                                        "user_id",
                                        "airline_id",
                                        "service_id"
                                    ]);

        $result = [];
        foreach ($staffFunctions as $each) {
            if (!isset($result[$each->user_id])){
                $result[$each->user_id] = [];
            }

            if (!isset($result[$each->user_id][$each->airline_id])){
                $result[$each->user_id][$each->airline_id] = [];
            }

            $result[$each->user_id][$each->airline_id][] = $each->service_id;
        }

        return $result;

    }

    public function prioritiesFirstServiceTime(&$list){

        if ($list && count($list)) {
            usort($list, array($this, "compareServiceReportTime"));
        }
    }

    function compareServiceReportTime($objA, $objB)
    {
        return $objA["first_service_time"] >= $objB["first_service_time"];
    }
}